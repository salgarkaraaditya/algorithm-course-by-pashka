#include <bits/stdc++.h>
using namespace std;
int main() {
  int n = 10; cin >> n;
  int m ; cin >> m;
  vector<vector<int>> adj(n);
  for(int i =0;i<m;i++){
    int u,v;
    cin>> u >> v;
    adj[u].push_back(v);
    adj[v].push_back(u);
    cout << u << ' ' << v << '\n';
  }

  vector<int> tin(n,-1), up(n);
  vector<int> vis(n, 0);
  vector<vector<int>> g(n);
  vector<pair<int,int>> bridges;
  int dfs_timer = 0;

  function<int(int, int)> dfs = [&](int node, int par) {
    // return up[node]
    vis[node]=1;
    tin[node] = ++dfs_timer;
    up[node] = tin[node];
    for(auto child:adj[node]){
      if(child==par) continue;
      if(vis[child]){
       up[node] = min(up[node],tin[child]);
      }
      else{
        up[node] = min(up[node], dfs(child,node));
      }
    }
    assert(par==-1 or tin[par]!=-1);
    if(par!=-1 and up[node] > tin[par]){
      bridges.push_back({par,node});
    }
    else{
      if(par!=-1){
        g[node].push_back(par);
        g[par].push_back(node);
      }
    }
    return up[node];
  };
  for (int i = 0; i < n; i++) {
    if (!vis[i]) {
      dfs(i, -1);
    }
  }
  cout << "bridges\n";
  for(auto [u,v]:bridges){
    cout<< u << ' ' << v << '\n';
  }

  cout << "tin -  up\n";
  for(int i = 0; i < n;i++){
    cout << i << ' '<< tin[i] << ' ' << up[i] << '\n'; 
  }
}
