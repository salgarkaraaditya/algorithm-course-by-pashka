%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Define Article %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Using Packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{empheq}
\usepackage{mdframed}
\usepackage{booktabs}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{color}
\usepackage{psfrag}
\usepackage{pgfplots}
\usepackage{bm}
\usepackage[]{algorithm2e}
\usepackage{minted}
\usepackage{hyperref}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Other Settings

%%%%%%%%%%%%%%%%%%%%%%%%%% Page Setting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\geometry{a4paper}

%%%%%%%%%%%%%%%%%%%%%%%%%% Define some useful colors %%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{ocre}{RGB}{243,102,25}
\definecolor{mygray}{RGB}{243,243,244}
\definecolor{deepGreen}{RGB}{26,111,0}
\definecolor{shallowGreen}{RGB}{235,255,255}
\definecolor{deepBlue}{RGB}{61,124,222}
\definecolor{shallowBlue}{RGB}{235,249,255}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% Define an orangebox command %%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\orangebox[1]{\fcolorbox{ocre}{mygray}{\hspace{1em}#1\hspace{1em}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% English Environments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheoremstyle{mytheoremstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\newtheoremstyle{myproblemstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowGreen,linecolor=deepGreen,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{theorem}{Theorem}[section]
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowBlue,linecolor=deepBlue,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{definition}{Definition}[section]
\theoremstyle{myproblemstyle}
\newmdtheoremenv[linecolor=black,leftmargin=0pt,innerleftmargin=10pt,innerrightmargin=10pt,]{problem}{Problem}[section]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Plotting Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepgfplotslibrary{colorbrewer}
\pgfplotsset{width=8cm,compat=1.9}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Title & Author %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{S03E03. Applications of DFS}
\title{\href{https://www.youtube.com/watch?v=W8hnuthPhWM&list=PLrS21S1jm43igE57Ye_edwds_iL7ZOAG4}{ S03E03. Applications of DFS}}
\author{Aaditya Salgarkar}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle

\section{Applications of dfs }
Undirected graphs have simple connectivity structure by an equivalence relation of $ u \sim v $, denoting that there is a path connecting $ u $ and $ v $.

Biconnectivity:
$ u \sim v $ are biconnected if there are two noninteresecting paths connecting $ u $ and $ v $.

There are two types:

1. Edge biconnectivity: when we allow the paths to have common nodes, but not common edges.

2. Vertex biconnectivity: when we don't allow common node.

\subsection{Edge biconnectivity}

This relation is transitive.
If you have paths from $ u - v $ and $ v-w $, it appears we can easily construct two paths by joining the respective paths. But, it is incorrect.

For instance, $ (uv)_1  $ and $ (uv)_2 $ don't intersect.
$ (vw)_1  $ and $ (vw)_2 $ don't intersect.
But, $ (uv)_1 $ may intersect $ (vw)_2 $.

Proof:
Since $ u \sim v $, there is a cycle $ u - (uv)_1 - v - (vu)_2 $.
Construct two paths from $ w $ to $ v $, $ (wv)_{1,2} $.

If they don't intersect $ (uv)_{1,2} $, we are done.
Suppose they do. Then, construct the first point when $ (wv)_i $ intersects
$ (uv)_j $ for some $ j $.
Call these points $ x,y $.
Then, we have two paths from $ w - u $.

$ (wu)_1  = (w- x - u)$

$ (wu)_2  = (w- x - v -(vu))$

Thus, this is an equivalence relation.

This divides the graph into biconnected components.

Note: Every cycle is in one biconnected component.

We can again construct a condensation for each component, which results in a forest!

Edges between different components are called `Bridges'.

Removing a bridge increases the number of \emph{connected components}.

Proof:
If the bridge is $ u-v $, $ u $ and $ v $ belong to different biconnected components.
Suppose there is another path joining $ u-v $ after removing this bridge.
Then, before removing there were two distinct paths from $ u -v $.
This contradicts with the definition of the bridge.

\subsection{Tarjan's Algorithm}

First, run dfs from any node.  We will get a dfs tree.

All the bridges will be in this tree.
Proof:
We start a dfs in one of the biconnected components.
We will move to all the other components.
The only way to go from one component to another is through a bridge.
Hence, we will encounter all the bridges in the dfs tree.

In fact, every spanning tree will contain all the bridges.


For every tree edge, we need to find if it is a bridge.
Consider $ v - p_v $ where $ p_v $ is the parent of $ v $.
If we remove this edge, the subtree rooted at $ v $ will be disconnected from the rest of the tree.

If this is not a bridge, then the subtree rooted at $ v $ will be connected to the rest of the tree through some other edge.

However, in a dfs tree, such an edge can't connect the descendants of $ v $ to the rest of the tree unless it is to $ u$, an ancestor of $ v $.

So, we need to find an edge from $ w $, a descendant of $ v $, to $ u $, an ancestor of $ v $.

We look for the topmost ancestor of $ v $ which is reachable from any $ w $.
This is possible since all the ancestors of $ v $ are on the same path to root.

During dfs, we can calculate the depth from the root of the tree for every node.

Alternatively, one can store the entry times. Higher entry time means deeper in the tree.
Slightly more efficient since all nodes have different entry times (unlike depths).
However, for the ancestors of $ v $, the ordering by depth and entry time is the same.


\begin{align}
  up[v]  = \min_{w | u \in g[w] | w \in S_v } \left( t_{in}[u] \right) .
\end{align}
Here, $ S_v $ denotes the set of descendants of $ v $ in the dfs tree.

$ v- p_v $ is not a bridge iff $ up[v]  \leq t_{in} (p_v)$.

We can compute $ up[v] $ using dynamic programming.

For $ v $, we just find the min of $ t_{in} $ of all the children of $ v $.
For each of the subtrees, we can find the min of $ up $ of all the children of $ v $.



\begin{listing}[ht!]
  \begin{minted}[frame=lines,linenos]{cpp}
  int n = 10; cin >> n;
  int m ; cin >> m;
  vector<vector<int>> adj(n);
  for(int i =0;i<m;i++){
    int u,v;
    cin>> u >> v;
    adj[u].push_back(v);
    adj[v].push_back(u);
    cout << u << ' ' << v << '\n';
  }

  vector<int> tin(n,-1), up(n);
  vector<int> vis(n, 0);
  vector<pair<int,int>> bridges;
  int dfs_timer = 0;

  function<int(int, int)> dfs = [&](int node, int par) {
    // return up[node]
    vis[node]=1;
    tin[node] = ++dfs_timer;
    up[node] = tin[node];
    for(auto child:adj[node]){
      if(child==par) continue;
      if(vis[child]){
       up[node] = min(up[node],tin[child]);
      }
      else{
        up[node] = min(up[node], dfs(child,node));
      }
    }
    assert(par==-1 or tin[par]!=-1);
    if(par!=-1 and up[node] > tin[par]){
      bridges.push_back({par,node});
    }
    return up[node];
  };
  for (int i = 0; i < n; i++) {
    if (!vis[i]) {
      dfs(i, -1);
    }
  }
  cout << "bridges\n";
  for(auto [u,v]:bridges){
    cout<< u << ' ' << v << '\n';
  }

  \end{minted}
  \caption{Tarjan's Algorithm}
  \label{lst:tarjan}
\end{listing}

%  input 

\begin{listing}
  \begin{minted}{bash}
10 12
0 6
0 8
1 2
1 6
2 6
3 6
3 9
4 9
4 7
5 6
5 9
6 8
\end{minted}

  \begin{minted}{bash}
bridges
4 7
9 4
  \end{minted}
  \caption{input}
\end{listing}

Finding components:

Remove the bridges and find the connected components.


\newpage
\section{Vertex Biconnected Components}

\emph{NOT} transitive

$ u \sim v $ and $ v \sim w $ does not imply $ u \sim w $ in general, since $ v $ can be a common vertex on the obvious path from $ u - w $.

Thus, there is no vertex partitioning of the graph.
But, we look for a edge partitioning of the graph.

This relation \emph{is} transitive.
Proof is similar to the proof for edge biconnected components.

There are two types of vertices.
1. If all the edges connected to this node belong to the same component.
2. Otherwise, called articulation points of the graphs.

Removing these articulation points increases the number of connected components.
Proof is similar to the bridges.

Condensation of the graph:

Wrong way: Make auxilary nodes for each connected component and then add edges if there is an articulation point joining them.

This might not lead to a tree since an articulation point can be in more than two connected components.

Correct way:
A node for each component

A node for each \emph{articulation point}

Add edges between a component and an AP.


\subsection{How to find articulation points (AP)?}

Consider $ v $ and its parents $ p_v $.

If it is not an AP, then there are edges in the subtree to complement.
But, dfs tree can have such edges only if they are $ v-w $, where $ w $ is an ancestor of $ v $.

The algorithm works the same as edge biconnected components, except for the root.

Root does not have any parent.
But, it is easy to check for the root since if there are more than one child in the root node, it is an AP.

TODO : How to find components using a stack.
\section{Euler tree}
\end{document}