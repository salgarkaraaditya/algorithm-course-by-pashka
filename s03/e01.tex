%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Define Article %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Using Packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{empheq}
\usepackage{mdframed}
\usepackage{booktabs}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{color}
\usepackage{psfrag}
\usepackage{pgfplots}
\usepackage{bm}
\usepackage[]{algorithm2e}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Other Settings

%%%%%%%%%%%%%%%%%%%%%%%%%% Page Setting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\geometry{a4paper}

%%%%%%%%%%%%%%%%%%%%%%%%%% Define some useful colors %%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{ocre}{RGB}{243,102,25}
\definecolor{mygray}{RGB}{243,243,244}
\definecolor{deepGreen}{RGB}{26,111,0}
\definecolor{shallowGreen}{RGB}{235,255,255}
\definecolor{deepBlue}{RGB}{61,124,222}
\definecolor{shallowBlue}{RGB}{235,249,255}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% Define an orangebox command %%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\orangebox[1]{\fcolorbox{ocre}{mygray}{\hspace{1em}#1\hspace{1em}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% English Environments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheoremstyle{mytheoremstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\newtheoremstyle{myproblemstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowGreen,linecolor=deepGreen,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{theorem}{Theorem}[section]
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowBlue,linecolor=deepBlue,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{definition}{Definition}[section]
\theoremstyle{myproblemstyle}
\newmdtheoremenv[linecolor=black,leftmargin=0pt,innerleftmargin=10pt,innerrightmargin=10pt,]{problem}{Problem}[section]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Plotting Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepgfplotslibrary{colorbrewer}
\pgfplotsset{width=8cm,compat=1.9}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Title & Author %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{ S03E01. Graphs. Depth First Search. Topological sorting}
\author{Aaditya Salgarkar}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
\section{Graphs}
\begin{definition}[Graph]
  A graph $G$ is a pair $(V,E)$ where $V$ is a set of vertices and $E$ is a set of edges. Each edge is a pair of vertices $(u,v)$ where $u,v \in V$.
\end{definition}

\begin{definition}[Directed Graph]
  A directed graph $G$ is a pair $(V,E)$ where $V$ is a set of vertices and $E$ is a set of edges. Each edge is an ordered pair of vertices $(u,v)$ where $u,v \in V$.
\end{definition}

\begin{definition}[Weighted Graph]
  A weighted graph $G$ is a pair $(V,E)$ where $V$ is a set of vertices and $E$ is a set of edges. Each edge is a pair of vertices $(u,v)$ where $u,v \in V$ and a weight $w(u,v)$.
\end{definition}

\begin{definition}[Path]
  A path in a graph $G$ is a sequence of vertices $v_1, v_2, \dots, v_k$ such that $(v_i, v_{i+1}) \in E$ for all $i \in \{1,2,\dots,k-1\}$.
\end{definition}
\begin{definition}[Simple Graph]
  A simple graph $G$ is a pair $(V,E)$ where $V$ is a set of vertices and $E$ is a set of edges. Each edge is a pair of vertices $(u,v)$ where $u,v \in V$ and $u \neq v$.

  A simple graph has no self loops and no parallel edges.
\end{definition}
$ n = |V| $, Number of vertices.

$ m = |E| $, Number of edges.

For simple, connected, undirected graphs, $ n-1 \leq m \leq \frac{n(n-1)}{2}$.
For simple, connected, directed graphs, $ n-1 \leq m \leq n(n-1)$.

\section{Connected components}

\begin{definition}[Connected vertices]
  Two vertices $u$ and $v$ in an undirected graph $G$ are connected if there is a path between $u$ and $v$.
\end{definition}

Note that this is an equivalence relation. It is reflexive, symmetric and transitive.

Proof of reflexivity:
\begin{proof}
  Let $u$ be a vertex in $G$. Then $(u,u)$ is a path of length 0 from $u$ to $u$. Hence $u$ is connected to $u$.
\end{proof}

Proof of symmetry:
\begin{proof}
  Let $u$ and $v$ be vertices in $G$ such that $u$ is connected to $v$. Then there is a path $P$ from $u$ to $v$. Then, for undirected graphs, $P$ is also a path from $v$ to $u$. Hence $v$ is connected to $u$.
\end{proof}

Proof of transitivity:
\begin{proof}
  Let $u$, $v$ and $w$ be vertices in $G$ such that $u$ is connected to $v$ and $v$ is connected to $w$. Then there is a path $P_1$ from $u$ to $v$ and a path $P_2$ from $v$ to $w$. Then, for undirected graphs, $P_1 \cup P_2$ is a path from $u$ to $w$. Hence $u$ is connected to $w$.
\end{proof}

This equivalence relation partitions the set of vertices into equivalence classes. Each equivalence class is called a connected component.

\begin{definition}[Connected components]
  A connected component of an undirected graph $G$ is a maximal set of vertices $C \subseteq V$ such that any two vertices in $C$ are connected by a path in $G$.
\end{definition}

Note that these definitions are for undirected graphs. For directed graphs, we have a similar definition of strongly connected components.

\section{Depth First Search}

Depth First Search is a graph traversal algorithm. It traverses the graph in a depth first manner. It starts at a vertex $v$ and explores as far as possible along each branch before backtracking.

Psuedocode:
% Use algorithm2e package

dfs (G, v):

mark v as visited

for each edge (v, u) in E:

if u is not visited: dfs(G, u)




\begin{theorem}
  Depth First Search visits all vertices in the connected component of $v$, and no other vertex in other connected components.
\end{theorem}

\begin{proof}
  Let $C$ be the connected component of $v$. Let $u$ be a vertex in $C$. We will show that $u$ is visited by DFS.

  Let $P$ be a path from $v$ to $u$. We will show that $u$ is visited by DFS.

  We will prove this by induction on the length of $P$.

  Base case: $P$ has length 0. Then $u = v$. Then $u$ is visited by DFS.

  Induction hypothesis: Assume that the theorem is true for all paths of length $k$.

  Induction step: Let $P$ be a path of length $k+1$. Then $P$ can be written as $P = (v, w) \cup P'$ where $P'$ is a path of length $k$ from $w$ to $u$. Then $w$ is visited by DFS. Then $u$ is visited by DFS.

  Hence, by induction, the theorem is true for all paths.

  On the other hand, let $u$ be a vertex not in $C$ and is visited in dfs. Then, there exists a vertex $ w $ such that $ u $ is called after $ w $.
  By induction, there is a path from $ u $ to $ v $, in contradiction to the fact that $ u $ is not in $ C $.

\end{proof}

Time complexity: $ O(n+m) $

For each vertex, dfs is called once.
Each edge is visited at most twice, once for each vertex.
Hence, the time complexity is $ O(n+m) $.

\subsection{Directed Graphs}
An important class of directed graphs are cyclic graphs. A directed graph is cyclic if there is a path from a vertex $v$ to itself.

If the graph is acyclic, then we can use DFS to find a topological ordering of the vertices.
Topological sorting is an ordering of the vertices such that for every edge $(u,v)$, $u$ comes before $v$ in the ordering.
In words, it is an ordering of the vertices such that every edge goes from left to right.

\begin{theorem}
  dfs visits the vertices in the reverse topological order.
\end{theorem}

At the end of each call of dfs, we append it to the list, and at the end, reverse the list to get the topological order.

\begin{proof}
  Let $ u,v $ be vertices visited by dfs such that $ u $ is called before $ v $, i.e., there is a directed edge from $ v  $ to $ u $.
  Then, $ u $ will be added to the list before $ v $, unless $ v $ is marked but not added to the list.
  In this case, there is a path $P  $ from $ u $ to $ v $.
  Now, $ P \cup Edge(v,u)$ forms a cycle, in contradiction to the fact that the graph is acyclic.
\end{proof}

\subsection{BFS based algorithm for topological sorting}
\begin{theorem}
  Let $ G $ be a directed acyclic graph. Then, there is a vertex $ v $ such that $ v $ has no incoming edges.
\end{theorem}
\begin{proof}
  Suppose if there does not exist any vertex with no incoming edges. Then, for every vertex $ v $, there is an incoming edge to $ v $.
  Then, we can construct a cycle by starting at any vertex and following the incoming edges.
\end{proof}

This gives us an algorithm for topological sorting by maintaining the set of vertices with no incoming edges.

\subsection{Detecting cycles}

Run the bfs based algorithm for topological sorting.
If there is a cycle, the final topological sorting will not cover all the vertices.

\pagebreak
Final algorithm:

mark all vertices as unvisited

dfs (G, v):
\begin{enumerate}
  \item mark v as on stack
  \item
        for each edge (v, u) in E:
        \begin{itemize}

          \item
                if u is not visited: Add (v,u) to the tree edges and dfs(G, u)

          \item
                if u is on stack: Add (v,u) to the back edges

          \item
                if u is not on stack: Add (v,u) to the forward or crossed edges
        \end{itemize}

  \item
        mask v as finished
\end{enumerate}
\end{document}