#include <bits/stdc++.h>
#include <functional>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/trie_policy.hpp>

#undef _GLIBCXX_DEBUG
#undef _GLIBCXX_DEBUG_PEDANTIC
using namespace std;
using namespace __gnu_pbds;
typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_multiset;

#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long , typename S> T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(v))
#define MAX(v) *max_element(ALL(v))
#define FOR(i,a,b) for(int i = (a) ; i < (b); i++ )
#define REP(a) FOR(_,0,a)
#define ROF(i,a,b) for(int i = (b)-1; i >= (a); i--)
template<class T, class U> T FIRSTTRUE(T lo, T hi, U f) {++hi; assert(lo <= hi); // assuming f is increasing
	while (lo < hi) { // find first index such that f is true
		T mid = lo + (hi - lo) / 2; f(mid) ? hi = mid : lo = mid + 1;
	}
	return lo;
}

using ll = long long;
using ull = unsigned long long int;
using i128 = __int128_t;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
using ld = long double;
template <typename T> using vc = vector<T>;
template <typename T> using vvc = vector<vc<T>>;
template <typename T> using vvvc = vector<vvc<T>>;
using vi = vc<int>;
using vl = vc<ll>;
using vpi = vc<pii>;
using vpl = vc<pll>;
template <class T> using pq = priority_queue<T>;
template <class T> using pqg = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S> inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S> inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }

const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n,q; cin>>n>>q;
    int LOG=1; while((1<<(LOG)) <=n) LOG++;
    vector<vi> A(LOG,vi(n,-1)),g(n); A[0][0]=-1; //A[j][i] = (1<<j)'th anc of i
    FOR(i,0,n-1){
      int from,to;cin>>from>>to;from--,to--;
      g[from].push_back(to),g[to].push_back(from);
    }
    vi parent(n,-1);
    vi depth(n,0);
    int timer=0; vi time_in(n),time_out(n);
    function<void(int,int)> dfs= [&](int node, int par){
      time_in[node]=timer++;
      if(par!=-1) depth[node]=depth[par]+1;
      parent[node]=par;
      dbg(node,par,depth[node]);
      for(auto child:g[node]) if(child!=par) dfs(child,node);
      time_out[node]=timer++;
    };
    dfs(0,-1);
    FOR(j,0,LOG) {
      FOR(i,1,n){
        if(j==0) {
          A[j][i]=parent[i];
        }
          else {A[j][i]=(A[j-1][i]!=-1?A[j-1][A[j-1][i]]:-1);}
      }
    }
    dbg(A);
    //REP(q){
    //  int node,jump;cin>>node>>jump;node--;
    //  dbg(node,jump);
    //  FOR(bit,0,LOG){
    //    if(node==-1 || jump==0){break;}
    //    if((jump>>bit) & 1){ node = A[bit][node];}
    //  }
    //  cout<<(node==-1?-1:node+1)<<endl;
    //}
    auto isAncestor = [&](int node, int subordinate){
      if (node==-1) return true;
      return time_in[subordinate] >= time_in[node] && time_out[subordinate] <= time_out[node];
    };
    dbg(g);
    dbg(time_in);
    dbg(time_out);
    auto lowestCommonAncestor = [&](int l, int r){
      if (isAncestor(l,r)) return l;
      if (isAncestor(r,l)) return r;
      ROF(jump,0,LOG) if(A[jump][l]!=-1 && !isAncestor(A[jump][l],r)){l=A[jump][l];}
      return A[0][l];
    };
    dbg(depth);
    REP(q)
    {
      int left,right; cin>>left>>right;left--,right--;
      dbg(left,right);
      cout<<depth[left]+depth[right]-2*depth[lowestCommonAncestor(left,right)]<<endl;
    }
    return 0;
}
