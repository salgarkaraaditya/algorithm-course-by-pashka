#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define ii pair<int, int>
#define vii vector<pair<int, int>>
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;
int DIR_X[] = {-1, 0, 1, 0};
int DIR_Y[] = {0, 1, 0, -1};

int n;
vector<vector<int>> g;
pair<int,vector<int>> findFarthest(int x)
{
    vector <int > dist;
    for (int i = 0; i < n ; i++){
        dist.push_back(n+10);
    }

    dist[x] = 0;
    stack<int> st;
    st.push(x);
    int farthest= x;
    while (!st.empty())
    {
        auto node = st.top();
        st.pop();
        for(auto nbr: g[node]){
            if(dist[nbr]!=n+10){continue;}
            st.push(nbr);
            dist[nbr] = dist[node] + 1;
            if(dist[nbr] > dist[farthest]) {
                farthest  = nbr;
            }

        }
    }
    return {farthest, dist};
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    for (int i = 0; i < n ; i++)
    {
        g.push_back({});
    }
    for (int i = 0; i < n - 1; i++)
    {
        int a, b;
        cin >> a >> b;
        a--;
        b--;
        g[a].push_back(b);
        g[b].push_back(a);
    }
    auto a = findFarthest(0);
    auto b = findFarthest(a.first);
    auto c = findFarthest(b.first);

    for (int i = 0; i < n ; i++){
        cout<< max(b.second[i],c.second[i])<<" ";
    }
    cout<<endl;




}