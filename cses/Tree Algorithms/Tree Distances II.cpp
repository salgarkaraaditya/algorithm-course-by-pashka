#include <bits/stdc++.h>
#include <functional>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/trie_policy.hpp>

#undef _GLIBCXX_DEBUG
#undef _GLIBCXX_DEBUG_PEDANTIC
using namespace std;
using namespace __gnu_pbds;
typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_multiset;

#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long , typename S> T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(v))
#define MAX(v) *max_element(ALL(v))
#define FOR(i,a,b) for(int i = (a) ; i < (b); i++ )
#define REP(a) FOR(_,0,a)
#define ROF(i,a,b) for(int i = (b)-1; i >= (a); i--)
template<class T, class U> T fstTrue(T lo, T hi, U f) {++hi; assert(lo <= hi); // assuming f is increasing
	while (lo < hi) { // find first index such that f is true
		T mid = lo + (hi - lo) / 2; f(mid) ? hi = mid : lo = mid + 1;
	}
	return lo;
}

using ll = long long;
using pii = pair<int, int>;
template <typename T> using vc = vector<T>;
template <typename T> using vvc = vector<vc<T>>;
using vi = vc<int>;
using vpi = vc<pii>;
template <class T> using pq = priority_queue<T>;
template <class T> using pqg = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S> inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S> inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }
int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n;cin>>n;
    vector<vi> g(n);
    FOR(i,0,n-1) {
      int a,b; cin>>a>>b;a--,b--;
      g[a].push_back(b),g[b].push_back(a);
    }
    vi sub(n); long long rootAns=0;
    function<void(int,int,int)> dfs= [&](int node, int par,int depth){
      sub[node]=1; rootAns+= (ll) depth;
      for(auto child:g[node]) if(child!=par) dfs(child,node,depth+1),sub[node]+=sub[child];
    };
    dfs(0,-1,0);
    dbg(sub);
    vector<ll> ans(n); ans[0]=rootAns;
    function<void(int,int)> dfs2= [&](int node, int par){
      for(auto child:g[node]) if(child!=par) ans[child]+= ans[node]+n-2*sub[child], dfs2(child,node);
    };
    dfs2(0,-1);
    FOR(i,0,n) cout<<ans[i]<<" \n"[i==n-1];
    return 0;
}
