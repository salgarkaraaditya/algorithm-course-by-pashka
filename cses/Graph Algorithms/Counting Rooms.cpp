#include <bits/stdc++.h>

using namespace std;

#define ar array
#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define REP(i, a, b) for (int i = int(a); i <= int(b); i++)
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;

// -------------------------<COMPARE>-------------------------
// Call by reference is used in x
template <typename T, typename U>
static inline void amin(T &x, U y)
{
    if (y < x)
        x = y;
}
// call by reference is used in x
template <typename T, typename U>
static inline void amax(T &x, U y)
{
    if (x < y)
        x = y;
}
//
// -------------------------<RNG>-------------------------
// RANDOM NUMBER GENERATOR
mt19937 RNG(chrono::steady_clock::now().time_since_epoch().count());
#define SHUF(v) shuffle(all(v), RNG);
// Use mt19937_64 for 64 bit random numbers.

template <typename T>
T gcd(T a, T b) { return (b ? __gcd(a, b) : a); }

template <typename T>
T lcm(T a, T b) { return (a * (b / gcd(a, b))); }

int add(int a, int b, int c = MOD)
{
    int res = a + b;
    return (res >= c ? res - c : res);
}
int mod_neg(int a, int b, int c = MOD)
{
    int res;
    if (abs(a - b) < c)
        res = a - b;
    else
        res = (a - b) % c;
    return (res < 0 ? res + c : res);
}
int mul(int a, int b, int c = MOD)
{
    ll res = (ll)a * b;
    return int(res >= c ? res % c : res);
}
int muln(int a, int b, int c = MOD)
{
    ll res = (ll)a * b;
    return int(((res % c) + c) % c);
}

template <typename T>
T expo(T e, T n)
{
    T x = 1, p = e;
    while (n)
    {
        if (n & 1)
            x = x * p;
        p = p * p;
        n >>= 1;
    }
    return x;
}
template <typename T>
T power(T e, T n, T m = MOD)
{
    T x = 1, p = e;
    while (n)
    {
        if (n & 1)
            x = mul(x, p, m);
        p = mul(p, p, m);
        n >>= 1;
    }
    return x;
}
template <typename T>
T extended_euclid(T a, T b, T &x, T &y)
{
    T xx = 0, yy = 1;
    y = 0;
    x = 1;
    while (b)
    {
        T q = a / b, t = b;
        b = a % b;
        a = t;
        t = xx;
        xx = x - q * xx;
        x = t;
        t = yy;
        yy = y - q * yy;
        y = t;
    }
    return a;
}
template <typename T>
T mod_inverse(T a, T n = MOD)
{
    T x, y, z = 0;
    T d = extended_euclid(a, n, x, y);
    return (d > 1 ? -1 : mod_neg(x, z, n));
}

// Permutation and Combination
// int fact[MAX_N],ifact[MAX_N];
// int ncr(int n,int r,int c = MOD){
// return mul(mul(ifact[r],ifact[n-r],c),fact[n],c);
//}

// ----------------------</MATH>--------------------------

/****************** Prime Generator **********************/
// const int N=1e7+10; int prime[20000010];
// bool isprime[N]; int nprime;
// template <typename T> void Sieve(T a)
//{nprime = 0;memset(isprime,true,sizeof(isprime));
// isprime[1]=false;for(int i=2;i<N;i++){
// if(isprime[i]){prime[nprime++]=i;for(int j=2;i*j<N;j++)
// isprime[i*j]=false;}}}

// template <typename T> bool miller_rabin(T p, T itt) {if(p<2) return 0 ;if(p==2) return 1;if(p%2==0) return 0 ;ull s = p-1 ;while(s%2==0) s/=2; for(ll i=1;i<=itt;i++) {ull a = rand() % (p-1)+1 , temp = s ; ull mod = modulo(a,temp,(ull)p);while(mod!=1 and mod!=p-1 and temp!=p-1){mod = Mulmod(mod,mod,(ull)p);temp*=2;} if(temp%2==0 and mod!=p-1) return false ;}return true;}

// template <typename T> inline T PrimeFactors(T n)
//{ll cnt=0,sum=1;for(int i=0; prime[i]*prime[i]<=n
// and i<nprime;i++){cnt=0;while(n%prime[i]==0)
//{cnt++;n/=prime[i];}sum*=(cnt+1);}
// if(n>1)sum*=2;return sum;}
/****************** Prime Generator End **********************/
int SZ = 1003;
int n, m;
char a[1003][1003];
int dx[5] = {-1, 0, 1, 0};
int dy[5] = {0, 1, 0, -1};
void floodFill(int ii, int ij)
{
    // cout << "floodFill " << i << ' ' << j << endl;

    a[ii][ij] = '#';

    REP(xx, 0, 3)
    {
        int x = ii + dy[xx];
        int y = ij + dx[xx];

        if (x >= 0 && x < n && y >= 0 && y < m && a[x][y] == '.')
        {
            // cout << "floodFill " << ii << ' ' << ij<<' '<<x<<' '<<y << endl;
            floodFill(x, y);
        }
    }
}
void solve()
{
    cin >> n >> m;
    //n == H and m == W 
    // cout<<n<<m<<endl;
    REP(i, 0, n - 1)
    REP(j, 0, m - 1)
    {
        cin >> a[i][j];
        // if(a[i][j]=='.'){
        //  cout<<"HOLE " << i << ' ' << j << endl;
        // }
    }

    int answer = 0;
    REP(i, 0, n - 1)
    REP(j, 0, m - 1)
    {
        // cout << i << ' ' << j << endl;

        if (a[i][j] != '.')
            continue;
        // cout << i << ' ' << j << endl;
        answer += 1;
        floodFill(i, j);
        // REP(ii,0,m-1) REP(jj,0,n-1){
        //     cout<<a[ii][jj];
        // }
        // cout<<endl;
    }
    cout << answer << '\n';
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int tc = 1;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
