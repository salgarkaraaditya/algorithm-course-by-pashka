#include <bits/stdc++.h>

using namespace std;

#define ar array
#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define REP(i, a, b) for (int i = int(a); i <= int(b); i++)
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;

// -------------------------<COMPARE>-------------------------
// Call by reference is used in x
template <typename T, typename U>
static inline void amin(T &x, U y)
{
    if (y < x)
        x = y;
}
// call by reference is used in x
template <typename T, typename U>
static inline void amax(T &x, U y)
{
    if (x < y)
        x = y;
}
//
int height, width;
int parent[MAX_N];
vector<int> answer;

vector<vector<int>> g;
void addEdge(int u, int v)
{
    // cout<<"Adding edge "<<u<<' '<<v<<endl;
    g[u].push_back(v);
    g[v].push_back(u);
}
int m, n;
int visited[MAX_N];
void solve()
{
    cin >> n >> m;
    REP(i, 0, n - 1)
    {
        g.push_back(vector<int>());
    }
    REP(i, 1, m)
    {
        int u, v;
        cin >> u >> v;
        u--;
        v--;
        addEdge(u, v);
    }
    // cout<<"Graph is"<<endl;
    // REP(i, 0, n - 1)
    // {
    //     cout<<i<<':'<<endl;
    //     for(auto it:g[i]){
    //         cout<<it<<endl;
    //     }
    // }
    /*
    do bfs starting at 0 and see if n-1 is reachable*/
    queue<int> que;
    que.push(0);
    parent[0] = 0;
    visited[0]=1;
    while (!que.empty())
    {

        int node = que.front();
        // cout<<"Visiting "<<node<<endl;
        if (node == (n - 1))
        {
            int curr = n - 1;
            while (parent[curr] != curr)
            {
                answer.push_back(curr);
                curr = parent[curr];
            }
            cout<<1+sza(answer)<<endl;
            cout<<1<<' ';
            reverse(all(answer));
            for(auto it:answer){
                cout<<it+1<<' ';
            }
            cout<<endl;
            return;
        }
        que.pop();

        for (auto next : g[node])
        {
            if (!visited[next])
            {
                visited[next] = 1;
                que.push(next);
                parent[next] = node;
            }
        }
    }
    cout<<"IMPOSSIBLE"<<endl;
    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int tc = 1;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
