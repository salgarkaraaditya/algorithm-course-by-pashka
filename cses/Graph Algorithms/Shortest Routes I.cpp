#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;

int height, width;
ll dist[MAX_N];
bool visited[MAX_N];
int n, m;
void showDist()
{
    cout << "******************************" << endl;
    for (int i = 0; i < n; i++)
    {
        // cerr << i << " : " << (dist[i] == LLONG_MAX ? -1 : dist[i]) << endl;
        cout << (dist[i] == LLONG_MAX ? -1 : dist[i]) << ',';
    }
    cout << endl;
    cout << "******************************" << endl;
}
void showQueue(priority_queue<pair<ll,int>> pq)
{
    auto internalCopy = pq;
    while (!internalCopy.empty())
    {
        auto tempVar = internalCopy.top();
        cout << -tempVar.first<<':' << tempVar.second<< ",";
        internalCopy.pop();
    }
    cout << endl;
}

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> n >> m;

    vector<vector<pair<int, ll>>> g(MAX_N);
    for (int i = 0; i < n; i++)
    {
        dist[i] = -1ll;
        visited[i] = false;
        // q.push(i);
    }
    for (int i = 0; i < m; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        u--, v--;

        g[u].push_back({v, w});
        // cout<<i<<endl;
    }
    priority_queue<pair<ll, int>> q;
    dist[0] = 0ll;
    q.push({0ll, 0});
    // int counter = 100;
    // cout<<"here"<<endl;
    while (!q.empty())
    {
        // showQueue(q);
        // counter--;
        // if (counter == 0)
        //     break;
        auto nodeData = q.top();
        auto node = nodeData.second;
        q.pop();
        if (visited[node])
        {
            continue;
        }
        visited[node] = true;
        if(sza(g[node])==0) continue;
        for (auto nbrWt : g[node])
        {
            auto nbr = nbrWt.first;
            auto wt = nbrWt.second;
            auto newDist = wt + dist[node];
            // cout << node << ' ' << nbr << ' ' << wt << endl;
            if (newDist < dist[nbr] || dist[nbr] == -1ll)
            {
                dist[nbr] = newDist;
                q.push({-dist[nbr], nbr});
            }
            // showDist();
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << dist[i] << ' ';
    }
    cout << endl;
    return 0;
}