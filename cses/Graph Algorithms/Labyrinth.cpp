#include <bits/stdc++.h>

using namespace std;

#define ar array
#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define REP(i, a, b) for (int i = int(a); i <= int(b); i++)
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;

// -------------------------<COMPARE>-------------------------
// Call by reference is used in x
template <typename T, typename U>
static inline void amin(T &x, U y)
{
    if (y < x)
        x = y;
}
// call by reference is used in x
template <typename T, typename U>
static inline void amax(T &x, U y)
{
    if (x < y)
        x = y;
}
//
// -------------------------<RNG>-------------------------
// RANDOM NUMBER GENERATOR
mt19937 RNG(chrono::steady_clock::now().time_since_epoch().count());
#define SHUF(v) shuffle(all(v), RNG);
// Use mt19937_64 for 64 bit random numbers.

template <typename T>
T gcd(T a, T b) { return (b ? __gcd(a, b) : a); }

template <typename T>
T lcm(T a, T b) { return (a * (b / gcd(a, b))); }

int add(int a, int b, int c = MOD)
{
    int res = a + b;
    return (res >= c ? res - c : res);
}
int mod_neg(int a, int b, int c = MOD)
{
    int res;
    if (abs(a - b) < c)
        res = a - b;
    else
        res = (a - b) % c;
    return (res < 0 ? res + c : res);
}
int mul(int a, int b, int c = MOD)
{
    ll res = (ll)a * b;
    return int(res >= c ? res % c : res);
}
int muln(int a, int b, int c = MOD)
{
    ll res = (ll)a * b;
    return int(((res % c) + c) % c);
}

template <typename T>
T expo(T e, T n)
{
    T x = 1, p = e;
    while (n)
    {
        if (n & 1)
            x = x * p;
        p = p * p;
        n >>= 1;
    }
    return x;
}
template <typename T>
T power(T e, T n, T m = MOD)
{
    T x = 1, p = e;
    while (n)
    {
        if (n & 1)
            x = mul(x, p, m);
        p = mul(p, p, m);
        n >>= 1;
    }
    return x;
}
template <typename T>
T extended_euclid(T a, T b, T &x, T &y)
{
    T xx = 0, yy = 1;
    y = 0;
    x = 1;
    while (b)
    {
        T q = a / b, t = b;
        b = a % b;
        a = t;
        t = xx;
        xx = x - q * xx;
        x = t;
        t = yy;
        yy = y - q * yy;
        y = t;
    }
    return a;
}
template <typename T>
T mod_inverse(T a, T n = MOD)
{
    T x, y, z = 0;
    T d = extended_euclid(a, n, x, y);
    return (d > 1 ? -1 : mod_neg(x, z, n));
}

// Permutation and Combination
// int fact[MAX_N],ifact[MAX_N];
// int ncr(int n,int r,int c = MOD){
// return mul(mul(ifact[r],ifact[n-r],c),fact[n],c);
//}

// ----------------------</MATH>--------------------------

/****************** Prime Generator **********************/
// const int N=1e7+10; int prime[20000010];
// bool isprime[N]; int nprime;
// template <typename T> void Sieve(T a)
//{nprime = 0;memset(isprime,true,sizeof(isprime));
// isprime[1]=false;for(int i=2;i<N;i++){
// if(isprime[i]){prime[nprime++]=i;for(int j=2;i*j<N;j++)
// isprime[i*j]=false;}}}

// template <typename T> bool miller_rabin(T p, T itt) {if(p<2) return 0 ;if(p==2) return 1;if(p%2==0) return 0 ;ull s = p-1 ;while(s%2==0) s/=2; for(ll i=1;i<=itt;i++) {ull a = rand() % (p-1)+1 , temp = s ; ull mod = modulo(a,temp,(ull)p);while(mod!=1 and mod!=p-1 and temp!=p-1){mod = Mulmod(mod,mod,(ull)p);temp*=2;} if(temp%2==0 and mod!=p-1) return false ;}return true;}

// template <typename T> inline T PrimeFactors(T n)
//{ll cnt=0,sum=1;for(int i=0; prime[i]*prime[i]<=n
// and i<nprime;i++){cnt=0;while(n%prime[i]==0)
//{cnt++;n/=prime[i];}sum*=(cnt+1);}
// if(n>1)sum*=2;return sum;}
/****************** Prime Generator End **********************/
int SZ = 1003;
int height, width;
char a[1003][1003];
int dx[5] = {-1, 0, 1, 0}; // R, D, L,U
int dy[5] = {0, 1, 0, -1};
int ax, ay, bx, by;
string answer;
vector<pair<int, int>> path;
// bool floodFill(int ii, int ij)
// {
//     // cout << "floodFill " << ii << ' ' << ij << endl;

//     a[ii][ij] = '#';

//     REP(xx, 0, 3)
//     {
//         int y = ii + dy[xx];
//         int x = ij + dx[xx];

//         // cout << "floodFill " << ii << ' ' << ij<<' '<<x<<' '<<y << endl;
//         if (x >= 0 && y < height && y >= 0 && x < width && (a[y][x] == '.' || a[y][x] == 'B'))
//         {
//             // cout << "floodFill " << ii << ' ' << ij<<' '<<x<<' '<<y << endl;
//             if (a[y][x] == 'B')
//             {
//                 path.push_back({y, x});
//                 // cout << y << ' ' << x << endl;
//                 return true;
//             }
//             if (floodFill(y, x))
//             {
//                 // cout << y << ' ' << x << endl;
//                 path.push_back({y, x});
//                 return true;
//             }
//         }
//     }
//     return false;
// }

pair<int, int> reverseHash(int hash)
{
    return {hash % height, int(hash / height)};
}
int Hash(int y, int x)
{
    return y + x * (height);
}
void solve()
{
    cin >> height >> width;

    // debug hash
    //  REP(i, 0, height - 1)
    //  REP(j, 0, width - 1)
    //  cout<<i<<' '<< j<<' '<<reverseHash(Hash(i,j))[0]<< ' ' << reverseHash(Hash(i,j))[1]<<endl;
    //  cout<<n<<m<<endl;

    REP(i, 0, height - 1)
    REP(j, 0, width - 1)
    {
        cin >> a[i][j];
        if (a[i][j] == 'A')
        {
            ay = i;
            ax = j;
        }
        if (a[i][j] == 'B')
        {
            by = i;
            bx = j;
        }
        // if(a[i][j]=='.'){
        //  cout<<"HOLE " << i << ' ' << j << endl;
        // }
    }

    /*dfs based code that doesn't give the shortest path

    if (floodFill(ay, ax))
    {
        cout << "YES\n";
        path.push_back({ay,ax});
        reverse(all(path));
        int length = sza(path);
        for(auto node:path){
            cout<<node.first<<' '<<node.second<<endl;
        }
        REP(i, 0, length - 2)
        {
            // cout << endl
                //  << path[i].first << ' ' << path[i].second << endl;
            int dirFirst = -path[i].first + path[i + 1].first;
            int dirSecond = -path[i].second + path[i + 1].second;

            if (dirFirst == -1)
            {
                cout << 'U';
            }
            else if (dirFirst == 1)
            {
                cout << 'D';
            }
            else if (dirSecond == -1)
            {
                cout << 'L';
            }
            else
                cout << 'R';
        }
        cout << endl;
    }
    else
    {
        cout << "NO\n";
    }
    */

    /*bfs based code starts here


    // bool flag = false;
    // queue<vector<int>> que;
    // que.push({ay, ax, 0});

    // while (!que.empty())

    // {

    //     queue<vector<int>> _next;
    //     // for each node in the que, populate the _next que with the direction as well!
    //     while (!que.empty())
    //     {
    //         vector<int> node = que.front();
    //         que.pop();

    //         int cury = node[0];
    //         int curx = node[1];

    //         int prevDir = node[2];
    //         char old = a[cury][curx];

    //         if (cury != ay && curx != ax)
    //             a[cury][curx] = "LDRU"[prevDir];

    //         if (old == 'B')
    //         {
    //             flag = true;

    //             cout << "YES\n";

    //             a[ay][ax] = 'A';
    //             char dir = a[by][bx];
    //             int pathy, pathx;
    //             pathy = by;
    //             pathx = bx;

    //             while (dir != 'A')
    //             {
    //                 cout << pathy << ' ' << pathx << endl;

    //                 // int dx[5] = {-1, 0, 1, 0};  //
    //                 // int dy[5] = {0, 1, 0, -1};

    //                 answer += dir;
    //                 int idx;
    //                 for (idx = 0; idx < 4; idx++)
    //                 {
    //                     if ("LDRU"[idx] == dir)
    //                     {
    //                         break;
    //                     }
    //                 }

    //                 pathy = pathy - dy[idx];
    //                 pathx = pathx - dx[idx];

    //                 dir = a[pathy][pathx];
    //             }
    //             for (int i = -1 + sza(answer); i >= 0; i--)
    //             {
    //                 cout << answer[i];
    //             }
    //             return;
    //         }
    //         for (int dir = 0; dir < 4; dir++)
    //         {
    //             int neighboury = cury + dy[dir];
    //             int neighbourx = curx + dx[dir];

    //             if (neighbourx >= 0 && neighboury < height && neighboury >= 0 && neighbourx < width && (a[neighboury][neighbourx] == '.' || a[neighboury][neighbourx] == 'B'))
    //             {
    //                 a[neighboury][neighbourx] = '#';
    //                 _next.push({neighboury, neighbourx, dir});
    //             }
    //         }
    //         // for(auto nextNode:_next){
    //         //     que.push(nextNode);
    //         // }
    //     }
    //     // debug

    //     // REP(ii, 0, width - 1) { cout << '-'; }
    //     // cout << endl;
    //     // REP(ii, 0, height - 1)
    //     // REP(jj, 0, width - 1)
    //     // {
    //     //     cout << a[ii][jj];
    //     //     cout << " \n"[int(jj == width - 1)];
    //     // }
    //     // enddebug
    //     que = _next;
    // }

    queue<int> que;
    // if (flag == false)
    // {
    //     cout << "NO\n";
    //     return;
    // }
    bfs based code ends here
    */

    queue<int> que;
    int parent[1001 * 1001 + 5];
    que.push(Hash(ay, ax));
    parent[Hash(ay, ax)] = Hash(ay, ax);
    bool found = false;
    while (!que.empty())
    {
        pair<int, int> node = reverseHash(que.front());
        que.pop();
        int prevY, prevX;
        prevY = node.first;
        prevX = node.second;

        // cout<<"Visiting "<<prevY<<  ' ' << prevX<<endl;

        REP(dir, 0, 3)
        {
            int nextY, nextX;

            nextY = prevY + dy[dir];
            nextX = prevX + dx[dir];

            if (nextY >= 0 && nextY < height && nextX >= 0 && nextX < width && (a[nextY][nextX] == 'B' || a[nextY][nextX] == '.')

            )
            {

                if (a[nextY][nextX] == 'B')
                    found = true;
                que.push(Hash(nextY, nextX));
                parent[Hash(nextY, nextX)] = Hash(prevY, prevX);
                a[nextY][nextX] = '#';
                if (found)
                {
                    cout << "YES" << endl;
                    // pair <int,int> ancestor = reverseHash(parent[Hash(nextY,nextX)]);
                    // cout<<ancestor.first << ' '<< ancestor.second<<endl;
                    int pathHash = Hash(by, bx);
                    path.push_back({by, bx});
                    while (pathHash != parent[pathHash])
                    {
                        pair<int, int> ancestor = reverseHash(parent[pathHash]);
                        // cout << ancestor.first << ' ' << ancestor.second << endl;
                        path.push_back(ancestor);
                        pathHash = parent[pathHash];
                    }
                    cout<<sza(path)-1<<endl;
                    for (int idx = 0; idx < sza(path); idx++)
                    {
                        int id = sza(path) - 1 - idx;
                        // cout << path[id].first << ' ' << path[id].second << endl;
                        if (idx < sza(path) - 1)
                        {
                            int dirY, dirX;
                            dirY = path[id].first - path[id - 1].first;
                            dirX = path[id].second - path[id - 1].second;
                            int dirIdx =0 ;
                            for(dirIdx = 0;dirIdx<4;dirIdx++){
                                if(-dirY==(dy[dirIdx]) && -dirX == (dx[dirIdx])){break;}
                            }

                            char directionLetter = "LDRU"[dirIdx];
                            // cout << "direction taken "<<dirY << ' '<<dirX <<" in words: "<<directionLetter<< endl;
                            cout<<directionLetter;
                        }
                    }
                    cout<<endl;
                    return;
                }
            }
        }
    }
    cout << "NO" << endl;
    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int tc = 1;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
