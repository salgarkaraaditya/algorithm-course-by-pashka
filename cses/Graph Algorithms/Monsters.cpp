#include <bits/stdc++.h>
//#include <functional>
//#include <ext/pb_ds/assoc_container.hpp>
//#include <ext/pb_ds/trie_policy.hpp>
#undef _GLIBCXX_DEBUG
#undef _GLIBCXX_DEBUG_PEDANTIC

using namespace std;
//using namespace __gnu_pbds;
//typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_multiset;

#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long , typename S> T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(V))
#define MAX(v) *max_element(ALL(v))
#define FOR(i,a,b) for(int i = (a) ; i < (b); i++ )
#define REP(a) FOR(_index,0,a)
#define ROF(i,a,b) for(int i = (b)-1; i >= (a); i--)
template<class T, class U> T FIRSTTRUE(T lo, T hi, U f) {++hi; assert(lo <= hi);
	while (lo < hi) { /*assuming f is increasing find first index such that f is true*/
		T mid = lo + (hi - lo) / 2; f(mid) ? hi = mid : lo = mid + 1;}
	return lo;}
using ll = long long;
using pii = pair<int, int>;
template <typename T> using vc = vector<T>;
using vi = vc<int>;
using vii = vc<pii>;
template <class T> using maxheap = priority_queue<T>;
template <class T> using minheap = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S> inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S> inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const string DIRECTIONS= "RULD";
void refresh(){
}

void solve(){
  int H,W;cin>>H>>W; dbg(H,W);
  vc<vi> g(H,vi(W));
  const int INF = 1000005;
  pii A,M;
  auto inside =[&](int pf,int ps){
    return (pf>=0 && pf<H && ps>=0 && ps<W);
  };
  vii monsters; vector<vector<vector<int>>> d(2,vector<vector<int>>(H,vector<int>(W,INF)));
  FOR(i,0,H) FOR(j,0,W) {
    char c;cin>>c; g[i][j]=int(c=='#');
    if(c=='A'){ A={i,j};d[0][i][j]=0;}
    if(c=='M'){ M={i,j};monsters.push_back(M);d[1][i][j]=0;}
  }
  //dbg(g);
  //dbg(A,M);
  const int dx[]= {-1,0,1,0}, dy[]={0,1,0,-1};
  auto bfs =[&](int type){
    queue<pii>q;
    if(type==0) q.push(A);
    else {
      for(auto pp:monsters){q.push(pp);}
    }
    while(!q.empty()){
      auto curr= q.front();q.pop();
      FOR(dir,0,4) {
        auto y=curr.first+dy[dir],x=curr.second+dx[dir];
        if(inside(y,x) && g[y][x]==0 && d[type][y][x]==INF){
          d[type][y][x]=1+d[type][curr.first][curr.second];
          q.push({y,x});
        }
      }
    }
  };
  bfs(0);
  bfs(1);
  //FOR(i,0,H){dbg(g[i]);}
  //FOR(i,0,H){dbg(i,d[0][i]);}
  //FOR(i,0,H){dbg(i,d[1][i]);}
  for(int i : {0,H-1}) FOR(j,0,W){
    if(d[1][i][j]>d[0][i][j]){
      dbg(i,j);
      cout<<"YES"<<endl;
      int tempy=i,tempx=j;
      vi path;
      while(d[0][tempy][tempx]){
        FOR(dir,0,4){
          auto currDist=d[0][tempy][tempx];
          auto nexty=tempy+dy[dir],nextx=tempx+dx[dir];
          if(!inside(nexty,nextx)) continue;
          auto nextDist= d[0][nexty][nextx];
          if(nextDist==currDist-1){
            path.push_back(dir);
            tempy=nexty,tempx=nextx;
            break;
          }
        }
      }
      REV(path);
      cout<<SZA(path)<<endl;
      for(auto dir:path){cout<<DIRECTIONS[dir];}
      return;
    }
  }
  for(int j : {0,W-1}) FOR(i,0,H){
      if(d[1][i][j]>d[0][i][j]){
        dbg(i,j);
        cout<<"YES"<<endl;
        int tempy=i,tempx=j;
        vi path;
        while(d[0][tempy][tempx]){
          FOR(dir,0,4){
            auto currDist=d[0][tempy][tempx];
            auto nexty=tempy+dy[dir],nextx=tempx+dx[dir];
            if(!inside(nexty,nextx)) continue;
            auto nextDist= d[0][nexty][nextx];
            if(nextDist==currDist-1){
              path.push_back(dir);
              tempy=nexty,tempx=nextx;
              break;
            }
          }
        }
        REV(path);
        cout<<SZA(path)<<endl;
        for(auto dir:path){cout<<DIRECTIONS[dir];}
        return;
      }
    }
  cout<<"NO"<<endl;
}

int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int testcases =1;
    //cin>>testcases;
    FOR(__testcase,1,1+testcases){
      dbg(__testcase);
     refresh(),solve();
    }
    return 0;
}
