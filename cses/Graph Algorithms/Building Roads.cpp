#include <bits/stdc++.h>

using namespace std;

#define ar array
#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define REP(i, a, b) for (int i = int(a); i <= int(b); i++)
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 1e9;
const ld EPS = 1e-9;

// -------------------------<COMPARE>-------------------------
// Call by reference is used in x
template <typename T, typename U>
static inline void amin(T &x, U y)
{
    if (y < x)
        x = y;
}
// call by reference is used in x
template <typename T, typename U>
static inline void amax(T &x, U y)
{
    if (x < y)
        x = y;
}
//
int height, width;
int parent[MAX_N];
int rk[MAX_N];
int independentSets;
void make_set(int v)
{
    parent[v] = v;
    rk[v] = 0;
}
int find_set(int v)
{
    if (v == parent[v])
        return v;
    return parent[v] = find_set(parent[v]);
}
void union_sets(int a, int b)
{
    a = find_set(a);
    b = find_set(b);
    if (a != b)
    {
        independentSets--;
        if (rk[a] < rk[b])
            swap(a, b);
        parent[b] = a;
        if (rk[a] == rk[b])
            rk[a]++;
    }
}
vector<vector<int>> g;
void addEdge(int u, int v)
{
    g[u].push_back(v);
    g[v].push_back(u);
    union_sets(u, v);
}
int m, n;
void solve()
{
    cin >> n >> m;
    independentSets = n;
    REP(i, 0, n - 1)
    {
        make_set(i);
        g.push_back(vector<int>());
    }
    REP(i, 1, m)
    {
        int u, v;
        cin >> u >> v;
        u--;
        v--;
        addEdge(u, v);
    }
    cout << independentSets - 1 << endl;
    vector<int> answer;
    REP(i, 0, n - 1)
    {
        // cout << i << ' ' << parent[i] << endl;
        if (parent[i] == i)
        {
            answer.push_back(i);
        }
    }
    REP(i, 0, independentSets - 2)
    {
        cout << answer[i]+1 << ' ' << answer[i + 1]+1 << endl;
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    int tc = 1;
    for (int t = 1; t <= tc; t++)
    {
        solve();
    }
}
