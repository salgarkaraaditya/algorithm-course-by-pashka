#include "bits/stdc++.h"
//#include <functional>
//#include <ext/pb_ds/assoc_container.hpp>
//#include <ext/pb_ds/trie_policy.hpp>

#undef _GLIBCXX_DEBUG
#undef _GLIBCXX_DEBUG_PEDANTIC
using namespace std;
//using namespace __gnu_pbds;
//typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_multiset;

#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long , typename S> T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(v))
#define MAX(v) *max_element(ALL(v))
#define FOR(i,a,b) for(int i = (a) ; i < (b); i++ )
#define REP(a) FOR(_,0,a)
#define ROF(i,a,b) for(int i = (b)-1; i >= (a); i--)
template<class T, class U> T fstTrue(T lo, T hi, U f) {++hi; assert(lo <= hi); // assuming f is increasing
	while (lo < hi) { // find first index such that f is true
		T mid = lo + (hi - lo) / 2; f(mid) ? hi = mid : lo = mid + 1;
	}
	return lo;
}

using ll = long long;
using pii = pair<int, int>;
template <typename T> using vc = vector<T>;
using vi = vc<int>;
using vpi = vc<pii>;
template <class T> using pq = priority_queue<T>;
template <class T> using pqg = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S> inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S> inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }

int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n,m;cin>>n>>m;
    vector<vi> g(n);vi indeg(n,0);
    //vi topSort(n,-1); queue<int> que;
    FOR(i,0,m){int x,y;cin>>x>>y;g[--x].push_back(--y),indeg[y]++;}
    dbg(g);
    // Topological sort using Kahn algorithm
    //FOR(i,0,n) if(indeg[i]==0) que.push(i);
    //int topSortId=0;
    //while(!que.empty()){
    //auto curr=que.front();que.pop();topSort[topSortId++]=curr;
    //for(auto node:g[curr]){ indeg[node]==1?que.push(node),0:indeg[node]--; }
    //}
    //if(topSortId!=n) cout<<"IMPOSSIBLE"<<endl;
    //else FOR(i,0,n) cout<<++topSort[i]<<" \n"[i==n-1];
    vector<int> marked(n,0);
    bool hasCycle=false;
    vector<int> topSort;
    function<bool(int)> dfs = [&](int node){
      bool ans=false;
      marked[node]=1;
      for(auto child:g[node]){
        if(marked[child]==0) {
          if(dfs(child)) return hasCycle=true;
        }
        else if(marked[child]==1) return hasCycle=true;
      }
      topSort.push_back(node);
      marked[node]=2;
      return ans;
    };
    FOR(i,0,n) if(marked[i]==0) dfs(i);
    dbg(topSort);
    if(hasCycle){cout<<"IMPOSSIBLE"<<endl; return 0;}

    REV(topSort);
    FOR(i,0,SZA(topSort)) cout<<++topSort[i]<<" \n"[i==n-1];
    return 0;
}
