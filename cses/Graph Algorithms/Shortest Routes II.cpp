#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

int height, width;
ll dist[MAX_N];
ll g[505][505];
int n, m, queries;

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> n >> m >> queries;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            g[i][j] = (INF);
        }
    }

    for (int i = 0; i < m; i++)
    {
        ll u, v, w;
        cin >> u >> v >> w;
        u--, v--;
        // if (u==v){
        //     continue;
        // }
        g[u][v] = min(w,g[u][v]);
        g[v][u] = min(w,g[v][u]);
    }
    for (int i = 0; i < n; i++)
    {
        g[i][i] = 0;
    }
    // process using ALLPAIRSHORTESTPATH
    for (int intermediate = 0; intermediate < n; intermediate++)
    {
        for (int from = 0; from < n; from++)
        {
            for (int to = 0; to < n; to++)
            {
                if (g[from][to] > g[from][intermediate] + g[intermediate][to])
                {
                    g[from][to] = g[from][intermediate] + g[intermediate][to];
                }
            }
        }
    }

    for (int i = 0; i < queries; i++)
    {
        int from, to;
        cin >> from >> to;
        from--, to--;
        cout << (g[from][to] == INF ? -1 : g[from][to]) << endl;
    }

    return 0;
}