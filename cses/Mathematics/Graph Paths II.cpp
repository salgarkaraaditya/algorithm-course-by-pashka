#pragma region template
#include <bits/stdc++.h>
using namespace std;

#ifdef LOCAL
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

char nl = '\n', sp = ' ';
string spnl = " \n";
#define len(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define lb(c, x) distance((c).begin(), lower_bound(all(c), (x)))
#define fr(i, a, b) for (int i = (a); i < (b); i++)
#define rep(a) fr(_index, 0, a)
#define rf(i, a, b) for (int i = (b)-1; i >= (a); i--)
using ll = long long;
using pii = pair<int, int>;
template <typename T>
using vc = vector<T>;
using vi = vc<int>;
template <class T, class S>
inline bool chmax(T& a, const S& b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S>
inline bool chmin(T& a, const S& b) { return (a > b ? a = b, 1 : 0); }
#pragma endregion

int solve() {
  int n;
  cin >> n;
  int m;
  cin >> m;
  int k;
  cin >> k;
  using Mat = vector<vector<ll>>;
  Mat matrix(n, vector<ll>(n, (0)));

  rep(m) {
    int u, v;
    cin >> u >> v;
    u--, v--;
    ll w;
    cin >> w;
    if (matrix[u][v] == 0) {
      matrix[u][v] = w;
    } else {
      chmin(matrix[u][v], w);
    }
  }
  dbg(matrix);

  // auto identity = Mat(n, vector<ll>(n, 0ll));
  // fr(i, 0, n) {
  //   identity[i][i] += 1ll;
  // }

  auto multiply = [&](Mat mat1, Mat mat2) {
    Mat temp(n, vector<ll>(n, 0ll));
    fr(i, 0, n) {
      fr(j, 0, n) {
        fr(q, 0, n) {
          ll curr = mat1[i][q] + mat2[q][j];
          if (mat1[i][q] == 0 || mat2[q][j] == 0) {
            continue;
          }
          if (temp[i][j]) {
            chmin(temp[i][j], curr);
          } else {
            temp[i][j] = curr;
          }
        }
      }
    }
    return temp;
  };

  auto ans = matrix;
  k -= 1;
  while (k) {
    if (k & 1) ans = multiply(matrix, ans);
    matrix = multiply(matrix, matrix);
    k >>= 1;
    dbg(k, ans);
  }

  dbg(ans);
  ll ret = ans[0][n - 1];
  if (ret == 0) ret -= 1;
  cout << (ret) << nl;
  return 0;
}

#pragma region main
int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int testcases = 1;
  // cin>>testcases;
  fr(__testcase, 1, 1 + testcases) {
    dbg(__testcase);
    solve();

    // cout<< (solve()?"Yes":"No")<<nl;
    // cout<< (solve()?"YES":"NO")<<nl;
  }
  return 0;
}
#pragma endregion