#pragma region template
#include <bits/stdc++.h>
using namespace std;

#ifdef LOCAL
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

char nl = '\n', sp = ' ';
string spnl = " \n";
#define len(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define lb(c, x) distance((c).begin(), lower_bound(all(c), (x)))
#define fr(i, a, b) for (int i = (a); i < (b); i++)
#define rep(a) fr(_index, 0, a)
#define rf(i, a, b) for (int i = (b)-1; i >= (a); i--)
using ll = long long;
using pii = pair<int, int>;
template <typename T>
using vc = vector<T>;
using vi = vc<int>;
template <class T, class S>
inline bool chmax(T& a, const S& b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S>
inline bool chmin(T& a, const S& b) { return (a > b ? a = b, 1 : 0); }
#pragma endregion

int solve() {
  int n;
  cin >> n;
  int k;
  cin >> k;

  vector<ll> a(n);
  fr(i, 0, n) {
    cin >> a[i];
  }
#pragma region binsearch
  using T = ll;
  auto firsttrue = [&](T lo, T hi, auto f) {
    ++hi;
    assert(lo <= hi && lo >= 0);
    while (lo < hi) { /*assuming f is increasing find first index such that f is true*/
      T mid = lo + (hi - lo) / 2;
      f(mid) ? hi = mid : lo = mid + 1;
    }
    return lo;
  };

  auto check = [&](ll x)-> bool {
    // is it NOT possible to have k subarrays with sum <=x 
    int cnt = 0;
    ll curr = 0ll;
    fr(i, 0, n) {
      if (a[i] + curr <= x) {
        curr += a[i];
      }
      else {
        cnt++;
        curr = a[i];
      }
    }
    cnt += (curr != 0);
    dbg(x, cnt);
    return cnt <= k;
  };
  auto ans = firsttrue(*max_element(all(a)), accumulate(all(a), 0ll), check);
#pragma endregion
  cout << ans << nl;



  return 0;
}

#pragma region main
int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int testcases = 1;
  // cin>>testcases;
  fr(__testcase, 1, 1 + testcases) {
    dbg(__testcase);
    solve();

    // cout<< (solve()?"Yes":"No")<<nl;
    // cout<< (solve()?"YES":"NO")<<nl;
  }
  return 0;
}
#pragma endregion