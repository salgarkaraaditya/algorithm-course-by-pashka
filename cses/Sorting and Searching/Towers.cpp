#include <bits/stdc++.h>
//#include <functional>
//#include <ext/pb_ds/assoc_container.hpp>
//#include <ext/pb_ds/trie_policy.hpp>
#undef _GLIBCXX_DEBUG
#undef _GLIBCXX_DEBUG_PEDANTIC

using namespace std;
//using namespace __gnu_pbds;
//typedef tree<int, null_type, less_equal<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_multiset;

#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long , typename S> T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(V))
#define MAX(v) *max_element(ALL(v))
#define FOR(i,a,b) for(int i = (a) ; i < (b); i++ )
#define REP(a) FOR(_index,0,a)
#define ROF(i,a,b) for(int i = (b)-1; i >= (a); i--)
template<class T, class U> T FIRSTTRUE(T lo, T hi, U f) {++hi; assert(lo <= hi);
	while (lo < hi) { /*assuming f is increasing find first index such that f is true*/
		T mid = lo + (hi - lo) / 2; f(mid) ? hi = mid : lo = mid + 1;}
	return lo;}
using ll = long long;
using pii = pair<int, int>;
template <typename T> using vc = vector<T>;
using vi = vc<int>;
using vii = vc<pii>;
template <class T> using maxheap = priority_queue<T>;
template <class T> using minheap = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S> inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S> inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;

void refresh(){
}

void solve(){
  int n;cin>>n;
  int ans=0;
  multiset<int> x;
  FOR(i,0,n){
    dbg(x);
    int z;cin>>z;
    auto id= x.upper_bound(z);
    if(id==(x.end())) x.insert(z);
    else {
      x.erase(id);
      x.insert(z);
    }
  }
  cout<<SZA(x)<<endl;
}

int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int testcases =1;
    //cin>>testcases;
    FOR(__testcase,1,1+testcases){
      dbg(__testcase);
     refresh(),solve();
    }
    return 0;
}
