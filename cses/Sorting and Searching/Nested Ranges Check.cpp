#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin >> n;

    vector<vector<int>> intervals;
    for (int i = 0; i < n; i++)
    {
        int l, r;
        cin >> l >> r;
        intervals.push_back({l, r, i});
    }
    sort(
        all(intervals),
        [&](vector<int> x, vector<int> y) -> bool
        {
            if (x[1] != y[1])
            {

                return x[1] < y[1];
            }
            else
            {
                return x[0] > y[0];
            }
        });

    // dbg(intervals);
    vector<int> ansFirst(n, 0);
    vector<int> ansSecond(n, 0);

    int prevMax = -1;
    for (auto interval : intervals)
    {
        if (prevMax >= interval[0])
        {
            ansFirst[interval[2]] = 1;
        }
        prevMax = max(interval[0], prevMax);
    }
    sort(
        all(intervals),
        [&](vector<int> x, vector<int> y) -> bool
        {
            if (x[0] != y[0])
            {
                return x[0] < y[0];
            }
            else
            {
                return x[1] > y[1];
            }
        });
    // reverse(all(intervals));
    // dbg(intervals);
    prevMax = -1;
    for (auto interval : intervals)
    {
        if (prevMax >= interval[1])
        {
            ansSecond[interval[2]] = 1;
        }
        prevMax = max(interval[1], prevMax);
    }
    for (auto var : ansFirst)
    {
        cout << var << ' ';
    }
    cout << endl;
    for (auto var : ansSecond)
    {
        cout << var << ' ';
    }
    cout << endl;

    return 0;
}