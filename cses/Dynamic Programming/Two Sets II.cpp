#include <bits/stdc++.h>

using namespace std;


constexpr int md = 1e9 + 7;

#define sza(x) ((int) x.size())
#define all(a) (a).begin(), (a).end()

vector<vector<int>> dp(505, vector<int>(505*215,0));
int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n; cin>>n;
    int target= n*(n+1)/2;
    if(target%2){ cout<<0<<endl;
      return 0;
    }
    target /=2;

    dp[0][0] = 1;
    for(int i =1 ; i < n;i++){
      for(int j = 0 ; j <=target; j++){
        dp[i][j] = dp[i-1][j] ;
        if(j>=i) dp[i][j] = (dp[i][j] + dp[i-1][j-i] )%md;
      }
    }
    cout<<dp[n-1][target]<<endl;
    return 0;
}
