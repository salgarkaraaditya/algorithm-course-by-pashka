#include <bits/stdc++.h>
using namespace std;

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define endl '\n'
#define DROP(s) cout << #s << endl, return
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define FOR(i, a, b) for (int(i) = (a); (i) < (b); (i)++)
#define SORT(v) sort(all(v))
#define REV(v) reverse(all(v))
#define UNIQUE(x) SORT(x), x.erase(unique(all(x)), x.end())
template <typename T = long long, typename S>
T SUM(const S &v) { return accumulate(all(v), T(0)); }
#define MIN(v) *min_element(ALL(v))
#define MAX(v) *max_element(ALL(v))

using ll = long long;
using ull = unsigned long long int;
using i128 = __int128_t;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
using ld = long double;
template <typename T>
using vc = vector<T>;
template <typename T>
using vvc = vector<vc<T>>;
template <typename T>
using vvvc = vector<vvc<T>>;
using vi = vc<int>;
using vl = vc<ll>;
using vpi = vc<pii>;
using vpl = vc<pll>;
template <class T>
using pq = priority_queue<T>;
template <class T>
using pqg = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S>
inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S>
inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }

const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

template <class Info,
          class Merge = std::plus<Info>>
struct SegmentTree {
  const int n;
  const Merge merge;
  std::vector<Info> info;
  SegmentTree(int n) : n(n), merge(Merge()), info(4 << std::__lg(n)) {}
  SegmentTree(std::vector<Info> init) : SegmentTree(init.size()) {
    std::function<void(int, int, int)> build = [&](int p, int l, int r) {
      if (r - l == 1) {
        info[p] = init[l];
        return;
      }
      int m = (l + r) / 2;
      build(2 * p, l, m);
      build(2 * p + 1, m, r);
      pull(p);
    };
    build(1, 0, n);
  }
  void pull(int p) {
    info[p] = merge(info[2 * p], info[2 * p + 1]);
  }
  void modify(int p, int l, int r, int x, const Info &v) {
    if (r - l == 1) {
      info[p] = v;
      return;
    }
    int m = (l + r) / 2;
    if (x < m) {
      modify(2 * p, l, m, x, v);
    } else {
      modify(2 * p + 1, m, r, x, v);
    }
    pull(p);
  }
  void modify(int p, const Info &v) {
    modify(1, 0, n, p, v);
  }
  Info rangeQuery(int p, int l, int r, int x, int y) {
    if (l >= y || r <= x) {
      return Info();
    }
    if (l >= x && r <= y) {
      return info[p];
    }
    int m = (l + r) / 2;
    return merge(rangeQuery(2 * p, l, m, x, y), rangeQuery(2 * p + 1, m, r, x, y));
  }
  Info rangeQuery(int l, int r) {
    return rangeQuery(1, 0, n, l, r);
  }
};

struct Info {
  int a;

  Info() : a{} {
    a = 0;
  }

  Info(int x) {
    a = x;
  }
};

Info operator+(Info a, Info b) {
  Info c;
  c.a = max(a.a, b.a);
  return c;
}

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int n;
  cin >> n;
  vector<int> b(n);
  FOR(i, 0, n) {
    cin >> b[i];
  }
  // cerr << "Main array" << '\n';
  // FOR(i, 0, n)
  // cerr << b[i] << " ";
  // cerr << '\n';
  SegmentTree<Info> S(n);
  vector<int> a = b;
  sort(a.begin(), a.end());
  // cerr << "Sorted array" << '\n';
  // FOR(i, 0, n)
  // cerr << a[i] << " ";
  // cerr << '\n';
  FOR(i, 0, n) {
    b[i] = distance(begin(a), lower_bound(begin(a), end(a), b[i]));
  }
  a = b;
  // cerr << "Coordinate compression" << '\n';
  // FOR(i, 0, n)
  // cerr << a[i] << " ";
  // cerr << '\n';
  vector<int> dp(n, 1);
  int ans = 1;
  // FOR(i,0,n){
  //   FOR(j,0,i){
  //    //among the smaller elements, find the highest dp value
  //     if(a[j]<a[i]){
  //       chmax(dp[i],dp[j]+1);
  //       chmax(ans,dp[i]);
  //     }
  //   }
  // }
  FOR(i, 0, n) {
    // find the max in the range 0 to a[i]
    chmax(dp[i], 1 + S.rangeQuery(0, a[i] ).a);
    S.modify(a[i], dp[i]);
    chmax(ans, dp[i]);
    // cerr << i << ' ' << a[i] << ' ' << dp[i] << '\n';
  }
  cout << ans << "\n";
}
