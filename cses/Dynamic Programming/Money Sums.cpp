#include <bits/stdc++.h>

using namespace std;

#define sza(x) ((int) x.size())
#define all(a) (a).begin(), (a).end()

int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);

    int n; cin>>n;
    vector<int> x(n); for(int i=0; i<n; i++)cin>>x[i]; sort(all(x));
    const int N  = 100 * 1000 + 5;
    vector<vector<bool>> dp(2, vector<bool>(N, false));
    dp[0][0] = true; dp[1][0] = true;

    for(int i =0 ; i < n ; i++ ){
      int par =   (i%2), coin = x[i];
      for(int sum = 0 ; sum < N ; sum++) {
        dp[par][sum] = (dp[par][sum] || (sum >= coin? dp[1- par][sum - coin]:false) || dp[1-par][sum]);
      }
    }
    vector<int> ans; for(int sum = 1; sum < N; sum++) if(dp[1 - n%2][sum]) ans.push_back(sum);

    cout<<sza(ans)<<endl;  for(auto y: ans ){cout<<y << ' ';} cout<<endl;
    return 0;
}
