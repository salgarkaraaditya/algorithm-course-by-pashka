#include <bits/stdc++.h>

using namespace std;

#define sza(x) ((int) x.size())
#define all(a) (a).begin(), (a).end()

const int N = 5005;
const long long INF = 5e12 + 5;
vector<vector<long long >> dp(N, vector<long long>(N,INF));
vector<long long> x(N);
long long find(int left,int right){
  if(dp[left][right]!=INF) return dp[left][right];
  if(left==right) return dp[left][right] = x[left];
  return dp[left][right] = max(x[left] - find(left+1,right) , x[right] - find(left,right-1));
}
int main()
{
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    int n; cin>>n; long long sum = 0;
    for(int i=0; i<n; i++)cin>>x[i],sum+=x[i];
    //for(int i =0; i < n ;i++) for(int j = i; j<n;j++) cout<<i<<' '<< j << ' ' << find(i,j)<<endl;
    cout<<(sum + find(0,n-1))/2<<endl;
    return 0;
}
