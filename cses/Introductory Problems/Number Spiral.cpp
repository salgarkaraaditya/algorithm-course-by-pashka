#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
auto find(int y, int x)
{
    if (x == y)
    {
        return 1ll + (ll)x * (x - 1);
    }
    if (y < x)
    {
        return find(x, x) + (y - x) * (x % 2 ? -1 : +1);
    }
    else
    {
        return find(y, y) + (y - x) * (y % 2 ? -1 : 1);
    }
}
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    while (n--)
    {
        int x, y;
        cin >> y >> x;
        cout << find(y, x) << endl;
    }
    return 0;
}