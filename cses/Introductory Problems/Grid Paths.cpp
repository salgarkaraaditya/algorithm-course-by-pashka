#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
const int dx[] = {0, 1, 0, -1};
const int dy[] = {1, 0, -1, 0};
const string DIRECTIONS = "DRUL";
int visited[8][8];
string s;
void showVisited()
{

    for (int h = 0; h < 7; h++)
    {
        for (int w = 0; w < 7; w++)
        {
            cout << (visited[h][w] ? "| " : ". ");
        }
        cout << endl;
    }
}
bool canGo(int i, int j)
{
    return !(i < 0 || j < 0 || i >= 7 || j >= 7 || visited[i][j] == 1);
}
bool check(int i, int j, int dir)
{
    // cout << "(" << i + dy[dir] << ',' << j + dx[dir] << ") : " << canGo(i + dy[dir], j + dx[dir]) << endl;
    return canGo(i + dy[dir], j + dx[dir]);
}
ll backtrack(int i, int j, int idx, int prev)
{
    // cout << DIRECTIONS[prev] << ' ' << i << ' ' << j << endl;
    // showVisited();
    ll temp = 0;

    if (i == 6 && j == 0)
    {
        return idx == 48 ? 1ll : 0ll;
    }
    bool canGoFront = check(i, j, prev), canGoLeft = check(i, j, (prev + 1) % 4), canGoRight = check(i, j, (prev + 3) % 4);
    if ((!canGoFront) && canGoLeft && canGoRight)
    {
        // cout << "DEADEND" << endl;
        return 0ll;
    }

    visited[i][j] = 1;
    if (canGoFront && (s[idx] == '?' || s[idx] == DIRECTIONS[prev]))
    {
        temp += backtrack(i + dy[prev], j + dx[prev], 1 + idx, prev);
    }
    prev += 1;
    prev %= 4;
    if (canGoLeft && (s[idx] == '?' || s[idx] == DIRECTIONS[prev]))
    {
        temp += backtrack(i + dy[prev], j + dx[prev], 1 + idx, prev);
    }
    prev += 2;
    prev %= 4;
    if (canGoRight && (s[idx] == '?' || s[idx] == DIRECTIONS[prev]))
    {
        temp += backtrack(i + dy[prev], j + dx[prev], 1 + idx, prev);
    }
    visited[i][j] = 0;
    // cout<<"BACK "<<i<<' '<<j<<endl;
    return temp;
}
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> s;

    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            visited[i][j] = 0;
        }
    }
    visited[0][0] = 1;
    ll ans = 0;
    if (s[0] == '?' || s[0] == 'D')
    {
        ans += backtrack(1, 0, 1, 0);
    }
    // cout << "" << endl;
    // cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << endl;
    // cout << "" << endl;

    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            visited[i][j] = 0;
        }
    }
    visited[0][0] = 1;

    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            visited[i][j] = 0;
        }
    }
    visited[0][0] = 1;
    if (s[0] == '?' || s[0] == 'R')
    {
        ans += backtrack(0, 1, 1, 1);
    }

    cout << ans << endl;

    return 0;
}