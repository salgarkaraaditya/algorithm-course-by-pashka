#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    ll ans = 0;
    vector<ll> a;
    for (int i = 0; i < n; i++)
    {
        int temp;
        cin >> temp;
        a.push_back(temp);
    }
    for (int i = 1; i < n; i++)
    {
        if (a[i] > a[i - 1])
        {
            continue;
        }
        ans -= a[i] - a[i - 1];
        a[i] = a[i - 1];
    }
    cout << ans << endl;

    return 0;
}