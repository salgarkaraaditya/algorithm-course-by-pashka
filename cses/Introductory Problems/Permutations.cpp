#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    if(n == 1){
        cout<< 1<< endl;
        return 0;
    }
    if (n < 4)
    {
        cout << "NO SOLUTION" << endl;
        return 0;
    }
    if(n==4){
        cout<<2<<' '<< 4<<' '<< 1<< ' ' <<3 <<endl;return 0;
    }
    int a[1000006];
    for (int i = 0; i < n; i++)
    {
        if (i % 2 == 0)
        {
            a[i] = i / 2 + 1;
        }
        else
        {
            a[i] = i / 2 + (n+1) / 2 +1 ;
        }
    }

    for (int i = 0; i < n; i++)
    {
        cout << a[i] << ' ';
    }
    cout << endl;

    return 0;
}