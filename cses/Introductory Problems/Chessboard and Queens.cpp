#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    vector<string> a;
    for (int i = 0; i < 8; i++)
    {
        string temp;
        cin >> temp;
        a.push_back(temp);
    }
    // for each row, store the column location
    vector<int> ans = {0, 1, 2, 3, 4, 5, 6, 7};
    ll answer = 0;

    do
    {
        set<int> diag1;
        set<int> diag2;
        for (int i = 0; i < 8; i++)
        {
            for (int j = ans[i]; j < 1+ans[i]; j++)
            {
                if (a[i][j] == '*')
                {
                    goto label;
                }
                if (diag1.find(i + j) != diag1.end() || diag2.find(i - j) != diag2.end())
                {
                    goto label;
                }
                diag1.emplace(i + j);
                diag2.emplace(i - j);
            }
        }
        // for(auto x:ans){
        //     cout<<x;
        // }
        // cout<<endl;
        answer += 1ll;
    label:
        continue;

    } while (next_permutation(all(ans)));
    cout << answer << endl;
    return 0;
}