#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    // n*(n+1) %4 :
    // 0 0, 1 2, 2 2, 3 0

    if (n % 4 == 1 || n%4==2) {
        cout << "NO" << endl;
        return 0;
    }

    cout << "YES" << endl;
    vector<int> f,s;
    if(n%4==0){
      for(int i=1;i<=n/2;i++){
        if(i%2) f.push_back(i),f.push_back(n+1-i);
        else  s.push_back(i),s.push_back(n+1-i);
      }
    }
    else{
      f={1,2},s={3};
      for(int i=4;i<=n;i++){
        if(i%4==3 || i%4==0){f.push_back(i);}
        else{s.push_back(i);}
      }
    }
    cout<<sza(f)<<endl;
    for(int i=0;i<sza(f);i++) cout<<f[i]<<" \n"[i==sza(f)-1];
    cout<<sza(s)<<endl;
    for(int i=0;i<sza(s);i++) cout<<s[i]<<" \n"[i==sza(s)-1];

    return 0;
}
