#include <bits/stdc++.h>
using namespace std;

#ifdef LOCAL
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define SZA(x) ((int)x.size())
#define ALL(a) (a).begin(), (a).end()
#define LB(c, x) distance((c).begin(), lower_bound(ALL(c), (x)))
#define TRV(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
#define SORT(v) sort(ALL(v))
#define REV(v) reverse(ALL(v))
#define UNIQUE(x) SORT(x), x.erase(unique(ALL(x)), x.end())
template <typename T = long long, typename S>
T SUM(const S &v) { return accumulate(ALL(v), T(0)); }
#define MIN(v) *min_element(ALL(V))
#define MAX(v) *max_element(ALL(v))
#define FOR(i, a, b) for (int i = (a); i < (b); i++)
#define REP(a) FOR(_index, 0, a)
#define ROF(i, a, b) for (int i = (b)-1; i >= (a); i--)
template <class T, class U>
T FIRSTTRUE(T lo, T hi, U f) {
  ++hi;
  assert(lo <= hi);
  while (lo < hi) { /*assuming f is increasing find first index such that f is true*/
    T mid = lo + (hi - lo) / 2;
    f(mid) ? hi = mid : lo = mid + 1;
  }
  return lo;
}
using ll = long long;
using pii = pair<int, int>;
template <typename T>
using vc = vector<T>;
using vi = vc<int>;
using vii = vc<pii>;
template <class T>
using maxheap = priority_queue<T>;
template <class T>
using minheap = priority_queue<T, vector<T>, greater<T>>;
template <class T, class S>
inline bool chmax(T &a, const S &b) { return (a < b ? a = b, 1 : 0); }
template <class T, class S>
inline bool chmin(T &a, const S &b) { return (a > b ? a = b, 1 : 0); }
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;

int main() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);

  int n, q;
  std::cin >> n >> q;

  vector<int> s(2 * n);
  std::vector<int> A(n);
  auto op = [&](int x, int y) { return min(x, y); };
  auto e = [&]() { return 1000000010; };
  auto build = [&]() {
    for (int i = n - 1; i >= 0; i--) {
      // important to traverse backwards
      s[i] = op(s[2 * i], s[2 * i + 1]);
    }
  };
  auto modify = [&](int i, int x) {
    A[i] = x;
    // reflect the change in the array
    i += n;
    // reflect the change in length one index
    s[i] = x;

    // pull to ancestors
    for (; i > 1; i >>= 1) {
      s[i >> 1] = op(s[i], s[i ^ 1]);
    }
  };
  auto rangeQuery = [&](int l, int r) {
    auto ans = e();
    for (l += n, r += n; l < r; l >>= 1, r >>= 1) {
      if (l & 1) ans = op(ans, s[l++]);
      if (r & 1) ans = op(ans, s[--r]);
    }
    return ans;
  };

  for (int i = 0; i < n; i++) {
    cin >> A[i];
    modify(i, A[i]);
  }
  build();
  dbg(s);

  FOR(i, 0, q) {
    int type, a, b;
    cin >> type >> a >> b;
    dbg(A);
    if (type == 2) {
      --a;
      // --b;
      cout << rangeQuery(a, b) << '\n';
    } else {
      modify(--a, b);
    }
  }
  return 0;
}
