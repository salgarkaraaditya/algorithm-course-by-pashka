#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

ll n, q;
vector<ll> Arr;
vector<ll> fen(2e5 + 5, 0ll);

void update(ll index, ll value)
{
    for (++index; index <= n; index += (index & (-index)))
    {
        fen[index] += value;
    }
}
ll rangeSumFromBeg(ll index)
{
    ll sum = 0;
    for (++index; index > 0; index -= (index & (-index)))
    {
        sum += fen[index];
    }
    return sum;
}
ll find(ll left, ll right)
{
    return rangeSumFromBeg(right) - (left ? rangeSumFromBeg(left - 1) : 0);
}
void showFen()
{
    for (int i = 0; i <= n; i++)
    {
        auto BITSET = bitset<16>(i);
        dbg(i, BITSET, fen[i]);
    }
}

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> n;
    cin >> q;
    for (int i = 0; i < n; i++)
    {
        ll temp;
        cin >> temp;
        Arr.push_back(temp);
        update(i, temp);
    }
    for (int queryNum = 1; queryNum <= q; queryNum++)
    {
        int type;
        ll a, b;
        cin >> type >> a >> b;

        if (type == 1)
        {
            a--;
            int tempA = int(a);
            update(tempA, b - Arr[tempA]);
            Arr[tempA] = b;
        }
        else
        {
            int tempA = int(a)-1;
            int tempB = int(b)-1;
            cout << find(tempA, tempB) << endl;
        }
    }
    return 0;
}