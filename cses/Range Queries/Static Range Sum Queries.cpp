#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, q;
    cin >> n;

    cin >> q;
    vector<ll> x;
    ll curr = 0;
    for (int i = 0; i < n; i++)
    {
        ll temp;
        cin >> temp;
        curr += temp;
        x.push_back(curr);
    }
    dbg(x);
    while (q--)
    {
        int a, b;
        cin >> a >> b;
        if (a == 1)
        {
            cout << x[b - 1] << endl;
        }
        else
        {
            cout << x[b - 1] - x[a - 2] << endl;
        }
    }
    return 0;
}