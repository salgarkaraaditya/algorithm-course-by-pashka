#include <bits/stdc++.h>
using namespace std;
#ifdef LOCAL
#define cerr cout
#include "myprettyprint.hpp"
#else
#define dbg(...)
#endif

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e6 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;
int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n, q;
    cin >> n;
    cin >> q;

    int sz = 0;
    int m = n;
    while (m)
    {
        m /= 2;
        sz++;
    }

    vector<ll> Arr;
    for (int i = 0; i < n; i++)
    {
        ll temp;
        cin >> temp;
        Arr.push_back(temp);
    }
    /*
    Precompute
    B[i][j] = index of the minimum of the range [i, i+1, ... , i + 2**j - 1]
    B[i][j-1] = [i, i+1, ... , i -1 + 2**(j-1)]
    B[i+2**j-1][j-1] = [i+2**(j-1), i+2**(j-1)+1, ... , i+2**(j-1) -1 + 2**(j-1)]
    */
    vector<vector<ll>>
        B(n, vector<ll>(sz, 0ll));
    for (int j = 0; j < sz; j++)
    {
        for (int i = 0; i < n; i++)
        {
            if (j == 0)
            {
                B[i][j] = i;
            }
            else
            {
                if (i + (1 << (j - 1)) >= n)
                {
                    continue;
                }
                B[i][j] = (Arr[B[i][j - 1]] <= Arr[B[i + (1 << (j - 1))][j - 1]]
                               ? B[i][j - 1]
                               : B[i + (1 << (j - 1))][j - 1]);
            }
        }
    }

    dbg(Arr);
    for (int i = 0; i < n; i++)
    {
        dbg(i, B[i]);
    }
    for (int queryNum = 1; queryNum <= q; queryNum++)
    {
        int a, b;
        cin >> a >> b;
        if (a == b)
        {
            cout << Arr[a - 1] << endl;
            continue;
        }
        /* find the largest power of two smaller than b-a
            then the answer is min(
                Arr[B[a][L]], Arr[B[b-L][b]]
            )
        */
        int v = 0;
        a--;
        b--;
        while (b - a >= (1 << (v + 1)))
        {
            v++;
        }
        int temp = 1 << v;
        int diff = b - a;
        dbg(queryNum);
        dbg(a, b, v, diff, temp);
        dbg(Arr[B[a][min(sz - 1, v)]], Arr[B[(b + 1 >= (1 << v)) ? (b + 1 - (1 << v)) : 0][min(sz - 1, v)]]);
        cout << min(Arr[B[a][min(sz - 1, v)]], Arr[B[(b + 1 >= (1 << v)) ? (b + 1 - (1 << v)) : 0][min(sz - 1, v)]]) << endl;
    }
    return 0;
}