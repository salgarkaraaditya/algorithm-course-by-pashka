#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define sza(x) ((int)x.size())
#define all(a) (a).begin(), (a).end()
#define endl '\n'
// <loops>
#define TRv(c, it) for (auto it = (c).begin(); it != (c).end(); it++)
//</loops>
#define PI 3.1415926535897932384626433832795l
const int MAX_N = 1e5 + 5;
const ll MOD = 1e9 + 7;
const ll INF = 500 * 1e9;
const ld EPS = 1e-9;

vector<ll> b1, b2;
ll findCount(ll t)
{

    // count numbers less than t+1 and subtract the numbers less than t

    return (lower_bound(all(b1), t + 1) - lower_bound(all(b1), t));
}

int main()
{

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    ll n, x;
    cin >> n >> x;

    ll a[45];
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    // Find all possible sums for a[0] to a[n/2], as well as a[n/2 + 1] to a[n-1]

    // n = 2k , partition as 0,...,k-1 and k,..., 2k-1
    // n = 2k-1 , partition as 0,...,k-1 and k,..., 2k-1
    for (ll bitmask = 0; bitmask < (1 << n / 2); bitmask++)
    {
        ll temp = 0;
        ll mask = bitmask;
        ll i = 0;
        // cout << mask << ' ';
        while (mask)
        {
            temp += a[i++] * (mask & 1);
            mask = mask / 2;
        }
        b1.push_back(temp);
        // cout << temp << endl;
    }
    for (ll bitmask = 0; bitmask < (1 << (n - (n / 2))); bitmask++)
    {
        ll temp = 0;
        ll mask = bitmask;
        ll i = n / 2;
        // cout << mask << ' ';
        while (mask)
        {
            temp += a[i++] * (mask & 1);
            mask = mask / 2;
        }
        b2.push_back(temp);
        // cout << temp << endl;
    }
    sort(all(b1));
    sort(all(b2));
    // for (auto num : b1)
    // {
    //     cout << num << endl;
    // }
    // cout << endl;
    // for (auto num : b2)
    // {
    //     cout << num << endl;
    // }

    // Now , we binary search for each element num in b2, if there is x- num in b1

    ll ans = 0;
    for (auto num : b2)
    {
        ans += findCount(x - num);
    }
    cout << ans << endl;
    return 0;
}