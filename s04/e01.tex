%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Define Article %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Using Packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{empheq}
\usepackage{mdframed}
\usepackage{booktabs}
\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{color}
\usepackage{psfrag}
\usepackage{pgfplots}
\usepackage{bm}
\usepackage{minted}
\usepackage[]{hyperref}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Other Settings

%%%%%%%%%%%%%%%%%%%%%%%%%% Page Setting %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\geometry{a4paper}

%%%%%%%%%%%%%%%%%%%%%%%%%% Define some useful colors %%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{ocre}{RGB}{243,102,25}
\definecolor{mygray}{RGB}{243,243,244}
\definecolor{deepGreen}{RGB}{26,111,0}
\definecolor{shallowGreen}{RGB}{235,255,255}
\definecolor{deepBlue}{RGB}{61,124,222}
\definecolor{shallowBlue}{RGB}{235,249,255}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%% Define an orangebox command %%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\orangebox[1]{\fcolorbox{ocre}{mygray}{\hspace{1em}#1\hspace{1em}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% English Environments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheoremstyle{mytheoremstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\newtheoremstyle{myproblemstyle}{3pt}{3pt}{\normalfont}{0cm}{\rmfamily\bfseries}{}{1em}{{\color{black}\thmname{#1}~\thmnumber{#2}}\thmnote{\,--\,#3}}
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowGreen,linecolor=deepGreen,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{theorem}{Theorem}[section]
\theoremstyle{mytheoremstyle}
\newmdtheoremenv[linewidth=1pt,backgroundcolor=shallowBlue,linecolor=deepBlue,leftmargin=0pt,innerleftmargin=20pt,innerrightmargin=20pt,]{definition}{Definition}[section]
\theoremstyle{myproblemstyle}
\newmdtheoremenv[linecolor=black,leftmargin=0pt,innerleftmargin=10pt,innerrightmargin=10pt,]{problem}{Problem}[section]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Plotting Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepgfplotslibrary{colorbrewer}
\pgfplotsset{width=8cm,compat=1.9}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Title & Author %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{E01 : Bipartite matching}
\author{Aaditya Salgarkar}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle
\href[]{ https://www.youtube.com/watch?v=4VYVnEcLZpQ&list=PLrS21S1jm43igE57Ye_edwds_iL7ZOAG4}{Link}

\section{Matching }

Undirected graph G is given.

\begin{definition}[Matching]
  A matching in G is a subset of edges of G such that no two edges share a common vertex.
\end{definition}

\begin{definition}[Maximum Matching]
  A matching is maximum if it has the maximum number of edges among all matchings.
\end{definition}

\begin{definition}[Perfect Matching]
  A matching is perfect if it covers all the vertices of G.
\end{definition}



A generic algorithm looks as follows:
1. start with an empty matching
2. while there is an augmenting path, add it to the matching

\begin{definition}[Augmenting Path]
  An augmenting path is a path that starts and ends at unmatched vertices and alternates between edges in the matching and edges not in the matching.
\end{definition}

The way to improve the matching is easy, just swap the matched/unmatched edges in the augmenting path.
There is always one more edge in the matching after the swap.


\begin{theorem}[Berge's Theorem]
  A matching is maximum if and only if there is no augmenting path.
\end{theorem}

\begin{proof}
  For any matching, if there is an augmenting path, then we can improve the matching by swapping the matched/unmatched edges in the augmenting path. This is true for any matching, so if there is a maximum matching, there is no augmenting path.

  For the other direction, we proceed by contradiction.
  $ M $ is our matching that is not maximal, and $ M_{max} $ is maximal.
  $ |M_{max}| > |M| $

  Remove all the edges which are only in $ M $ or $ M_{max} $, and call the resulting graph $ G' $.

  For each node, the degree in $ G' $ is at most 2. This is because the degree in $ M $ is at most 1, and the degree in $ M_{max} $ is at most 1. So, the degree in $ G' $ is at most 2.

  Such a graph has a special structure. Each connected component is either a cycle or a chain.
  Color the edges in $ M $ red, and the edges in $ M_{max} $ blue.
  Then, if there is a cycle, it has to alternate between red and blue edges. This is because the degree of each node is at most 2, so it cannot have two consecutive edges of the same color.
  But, we have $ |M_{max}| > |M|$.

  So, there must be at least one component which is a chain.
  The chain also has to alternate between red and blue edges.
  This is precisely an augmenting path in $ M $.
\end{proof}

We consider the problem for bipartite graphs.
It is easy to find an augmenting path for bipartite graphs.
Let's give a direction the edges in the matching to be right to left, and the rest to be left to right.

Introduce a source and a sink. Source connects to all the left nodes with directed edges from the source and sink connects to all the right nodes with directed edges ending in the sink.

The augmenting path is any path from the source to the sink.

Complexity is $O(nm) $ where n is the number of nodes in the graph and m is the number of edges.
This is because each augmenting path increases the matching by 1. There is at most n matchings. Each augmenting path can be found in $O(m) $ time using graph search.


This can be implemented without building the directed graph explicitly.

We use the fact that for each node on the right half that is not in the matching, there is exactly one edge that is in the matching leaving from it.

So, we can run dfs only from the left side nodes until we find a node on the right side that is not in the matching.
Then, we can backtrack to find the augmenting path.

\begin{minted} [ frame=lines, framesep=2mm, baselinestretch=1.2, bgcolor=shallowGreen, fontsize=\footnotesize, linenos ] {c++}
bool dfs(int u) {
    if (vis[u]) return false;
    vis[u] = true;
    for (int v : adj[u]) {
        if (match[v] == -1 || dfs(match[v])) {
            match[v] = u;
            return true;
          }
      }
    return false;
  }
for (int i = 0; i < n; i++) {
  vis.assign(n, false);
  total_matching += dfs(i);
}
\end{minted}

\begin{theorem}[Hall's Theorem]
  A bipartite graph has a perfect matching if and only if for every subset of nodes on the left side, the number of neighbors is at least the size of the subset.
\end{theorem}

\begin{proof}
  Denote the right neighbors of the subset $ A $ on the left as $ N(A) $.
  If $ |N(A_{all})| < |A_{all}| $, then there are not enough neighbors for each node in $ A $, so there cannot be a perfect matching.

  TODO: show that each dfs can not stop until it finds an unmatched node on the right side.
\end{proof}

\begin{definition}[Vertex cover]
  A vertex cover is a subset of nodes such that every edge has at least one endpoint in the subset.
\end{definition}

For generic graphs, this is NP-complete. But for bipartite graphs, it is easy to find a minimum vertex cover.

We use the fact that, for bipartite graphs, the size of the minimum vertex cover is equal to the size of the maximum matching.

Define $ L_+, L_-,R_+, R_- $, as subsets of nodes on the left and right side.
$ L_+ $ is the set of nodes that are not in the matching and reachable from those not in the matching.
$ L_- $ is the rest of the nodes on the left side.
$ R_+ $ is the set of nodes that are reachable from $ L_+ $ using dfs above.
$ R_- $ is the set of nodes that are not reachable from $ L_+ $ using dfs above.

All the free nodes are in $ L_+,R_- $.


We can't have an edge from $ L_+ $ to $ R_- $, because then we can find an augmenting path of length 1.
We also can't have an edge from $ R_- $ to $ L_+ $, because that would contradict matching. (Why? Because if there is such an edge $ u-v $).

Only possible edges are from $ L_- $ to $ R_+ $.

Minimum vertex cover is $ L_- \cup R_+ $.
It is minimum because its size is the same as the size of the maximum matching, beacuse maximum matching is precisely the edges from $ L_+ - R_+ $ and $ L_- - R_- $.
\end{document}