# run all the Makefiles in the subdirectories

SUBDIRS = $(shell ls -d */)

MAKE = make 

all:
	@for i in $(SUBDIRS); do \
		echo "Making all in $$i"; \
		(cd $$i; $(MAKE) all); \
	done