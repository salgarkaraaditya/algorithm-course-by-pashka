// AVL tree with insert, find and delete operations
#include <bits/stdc++.h>
using namespace std;

struct TreeNode {
  int val;          // value of the node
  int height;       // height of the subtree
  int balance;      // balance = height(left) - height(right)
  TreeNode* left;   // left child
  TreeNode* right;  // right child
  TreeNode(int x) : val(x), height(1), left(NULL), right(NULL), balance(0) {}
};

// Root node
TreeNode* root = NULL;

void check_consistency(TreeNode* node) {
  if (node == NULL) return;
  if (node->left != NULL) {
    assert(node->left->val < node->val);
    check_consistency(node->left);
  }
  if (node->right != NULL) {
    assert(node->right->val > node->val);
    check_consistency(node->right);
  }

  assert(node->balance == node->left->height - node->right->height);
  assert(node->height == max(node->left->height, node->right->height) + 1);
  assert(abs(node->balance) <= 1);
}

TreeNode* insert(int x, TreeNode* current) {
  if (current == NULL) {
    current = new TreeNode(x);
    return current;
  }
  assert(current != NULL);
  if (current->val < x) {
    if (current->right == NULL) {
      current->right = new TreeNode(x);
    } else {
      current->right = insert(x, current->right);
    }
  } else {
    if (current->left == NULL) {
      current->left = new TreeNode(x);
    } else {
      current->left = insert(x, current->left);
    }
  }

  return current;
}
void remove(int x) {
}
TreeNode* find(int x, TreeNode* current) {
  if (current == NULL) return NULL;
  if (current->val == x) return current;
  if (current->val < x) return find(x, current->right);
  return find(x, current->left);
}

void print_tree(TreeNode* current) {
  if (current == NULL) return;
  print_tree(current->left);
  cout << current->val << " ";
  print_tree(current->right);
}
int main() {
  int queries;
  cin >> queries;
  while (queries--) {
    check_consistency(root);
    print_tree(root);

    int type;
    cin >> type;

    if (type == 1) {
      int x;
      cin >> x;
      insert(x, root);
    } else if (type == 2) {
      int x;
      cin >> x;
      remove(x);
    } else if (type == 3) {
      int x;
      cin >> x;
      auto answer = find(x, root);
      cout << (answer == NULL ? "NO" : "YES") << endl;
    }
  }
}